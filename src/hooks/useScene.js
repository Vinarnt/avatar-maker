import SceneManager from '@services/SceneManager';
import store from '@store/store';

function useScene() {
    function removeSelectedActors() {
        SceneManager.removeActors(
            store.getState().scene.selectedActors.slice()
        );
    }

    return {
        removeSelectedActors: removeSelectedActors
    };
}

export default useScene;
