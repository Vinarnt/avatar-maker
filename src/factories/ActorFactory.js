import { fabric } from 'fabric';

import SceneManager from '@services/SceneManager';
import store from '@store/store';
import subscribe from '@store/subscriber';
import settingIdentifiers from '@constants/settings';
import { capitalize } from '@utils/string';
import { _initFabricObject } from '@utils/actor';
import { loadImageFromFile } from '@utils/image';

const { dispatch } = store;

const ActorFactory = {
    _currentId: 0,
    createShape(type, color, x, y) {
        if (typeof type !== 'string')
            throw new Error('type argument should be a string');

        let fabricObject;
        switch (type) {
            case 'rectangle':
                fabricObject = this._createFabricRectangle(color, x, y);
                break;
            case 'triangle':
                fabricObject = this._createFabricTriangle(color, x, y);
                break;
            case 'circle':
                fabricObject = this._createFabricCircle(color, x, y);
                break;
            case 'ellipse':
                fabricObject = this._createFabricEllipse(color, x, y);
                break;
            default:
                throw new Error(`Can't create actor shape of type: ${type}`);
        }

        const shape = this._createShape(type);
        _initFabricObject(SceneManager.canvas, fabricObject);
        shape.fabricObject = fabricObject;
        fabricObject.actor = shape;

        if (x === undefined || x === null || y === undefined || y === null)
            this._centerOnAdd(shape);

        return shape;
    },
    createImage(name, source, x, y, onLoadCallback) {
        switch (typeof source) {
            case 'object':
                return this._createImageFromFile(
                    name,
                    source,
                    x,
                    y,
                    onLoadCallback
                );
            default:
                throw new Error(`Source type not supported: ${typeof source}`);
        }
    },
    createText(content, x, y, color = 'white') {
        const text = this._createActor('text', 'Text');
        const fabricObject = new fabric.Textbox(content, {
            left: x,
            top: y,
            width: 150,
            height: 45,
            fill: color
        });
        _initFabricObject(SceneManager.canvas, fabricObject);
        text.fabricObject = fabricObject;
        fabricObject.actor = text;

        if (x === undefined || x === null || y === undefined || y === null)
            this._centerOnAdd(text);

        return text;
    },
    createBorder: function(width = 5) {
        const settings = store.getState().settings;
        const imageShape = settings[settingIdentifiers.general.image.shape];
        const imageSize = settings[settingIdentifiers.general.image.size];

        const border = this._createActor('border', 'Border');
        const fabricObject = this._createBorderShape(imageShape, imageSize);
        fabricObject.clipPath = this._createBorderClipPath(
            imageShape,
            imageSize,
            width
        );

        border.width = width;
        border.shape = imageShape;
        border.fabricObject = fabricObject;
        fabricObject.actor = border;

        this._setupBorderSubscriber(border);
        this._centerOnAdd(border);

        return border;
    },
    _createActor: function(type, name, others) {
        return {
            id: this._currentId++,
            type: type,
            name: name,
            ...others
        };
    },
    _createImageFromFile: function(name, file, x, y, onLoadCallback) {
        const image = this._createImageActor(name);

        loadImageFromFile(file).then(imageData =>
            this._loadImageActor(image, imageData, x, y, onLoadCallback)
        );

        return image;
    },
    _createImageActor: function(name) {
        return this._createActor('image', name, { loaded: false });
    },
    _loadImageActor: function(
        imageActor,
        url,
        x,
        y,
        onLoadCallback = () => {}
    ) {
        const imageElement = document.createElement('img');
        imageElement.crossOrigin = 'Anonymous';
        imageElement.id = `image-${imageActor.id}`;
        imageElement.classList.add('hidden');
        document.querySelector('.lower-canvas').appendChild(imageElement);

        imageElement.addEventListener(
            'load',
            e => {
                const fabricObject = new fabric.Image(e.target, {
                    left: x || 0,
                    top: y || 0
                });
                _initFabricObject(SceneManager.canvas, fabricObject);
                imageActor.fabricObject = fabricObject;
                fabricObject.actor = imageActor;

                SceneManager.canvas.add(fabricObject);
                fabricObject.centeredScaleToHeight(
                    SceneManager.canvas.clipPath.height
                );

                if (
                    x === undefined ||
                    x === null ||
                    y === undefined ||
                    y === null
                )
                    fabricObject.center();

                const resizeFilter = new fabric.Image.filters.Resize();
                // Add a filter to avoid pixelating when scaling
                const applyResizeFilter = (scaleX, scaleY) => {
                    resizeFilter.scaleX = scaleX;
                    resizeFilter.scaleY = scaleY;

                    fabricObject.applyFilters();
                };
                fabricObject.filters.push(resizeFilter);
                fabricObject.on(
                    'scaled',
                    ({ transform: { scaleX, scaleY } }) => {
                        applyResizeFilter(scaleX, scaleY);
                    }
                );
                applyResizeFilter(fabricObject.scaleX, fabricObject.scaleY);

                SceneManager.requestRenderAll();

                imageActor.loaded = true;
                dispatch.scene.updateActors();

                onLoadCallback(imageActor);
            },
            { once: true }
        );

        imageElement.src = url;
    },
    _createShape: function(type) {
        const actor = this._createActor('shape', capitalize(type));
        actor.shape = type;

        return actor;
    },
    _createFabricRectangle: function(color, x = 0, y = 0) {
        return new fabric.Rect({
            left: x,
            top: y,
            width: 50,
            height: 50,
            fill: color
        });
    },
    _createFabricTriangle: function(color, x = 0, y = 0) {
        return new fabric.Triangle({
            left: x,
            top: y,
            width: 50,
            height: 50,
            fill: color
        });
    },
    _createFabricCircle: function(color, x = 0, y = 0) {
        return new fabric.Circle({
            left: x,
            top: y,
            radius: 50,
            fill: color
        });
    },
    _createFabricEllipse: function(color, x = 0, y = 0) {
        return new fabric.Ellipse({
            left: x,
            top: y,
            rx: 50,
            ry: 30,
            fill: color
        });
    },
    _createBorderShape: function(shape, size, color = 'purple') {
        let fabricObject;
        switch (shape) {
            case 'circle':
                fabricObject = new fabric.Circle({
                    radius: size.radius + 1 // Add 1 to overcome floating point inconsistency
                });
                break;
            case 'rectangle':
                fabricObject = new fabric.Rect({
                    width: size.width + 2, // Add 2 to overcome floating point inconsistency
                    height: size.height + 2 // Add 2 to overcome floating point inconsistency
                });
                break;
            default:
                throw new Error(`Unhandled type: ${shape}`);
        }
        Object.assign(fabricObject, {
            originX: 'center',
            originY: 'center',
            centeredScaling: true,
            evented: false,
            selectable: false,
            hasControls: false,
            fill: color
        });

        return fabricObject;
    },
    _createBorderClipPath: function(shape, size, width) {
        let clipPath;
        switch (shape) {
            case 'circle':
                clipPath = new fabric.Circle({
                    radius: Math.max(size.radius - width, 0)
                });
                break;
            case 'rectangle':
                clipPath = new fabric.Rect({
                    width: size.width - width * 2,
                    height: size.height - width * 2
                });
                break;
            default:
                throw new Error(`Unhandled type: ${shape}`);
        }
        clipPath.set({
            originX: 'center',
            originY: 'center',
            inverted: true
        });

        return clipPath;
    },
    _setupBorderSubscriber: function(border) {
        // Listen to clipPath modifications to reflect on the border
        const unsubscribe = subscribe(
            'settings',
            null,
            (oldSettings, newSettings) => {
                let oldImageShape = oldSettings
                    ? oldSettings[settingIdentifiers.general.image.shape]
                    : undefined;
                let oldImageSize = oldSettings
                    ? oldSettings[settingIdentifiers.general.image.size]
                    : undefined;
                let newImageShape =
                    newSettings[settingIdentifiers.general.image.shape];
                let newImageSize =
                    newSettings[settingIdentifiers.general.image.size];
                const needShapeUpdate =
                    oldImageShape && oldImageShape !== newImageShape;
                const needSizeUpdate =
                    !needShapeUpdate &&
                    oldImageSize &&
                    oldImageSize !== newImageSize;
                let needUpdate = false;
                let borderFabricObject = border.fabricObject;
                const borderWidth = border.width;
                const borderColor = borderFabricObject.fill;

                // Update border type
                if (needShapeUpdate) {
                    // This line is running a subscribe call in parallel, because of the selection clear
                    SceneManager.canvas.remove(borderFabricObject);
                    // TODO: Restore selection if needed
                    borderFabricObject = this._createBorderShape(
                        newImageShape,
                        newImageSize,
                        borderColor
                    );
                    borderFabricObject.clipPath = this._createBorderClipPath(
                        newImageShape,
                        newImageSize,
                        borderWidth
                    );

                    border.fabricObject = borderFabricObject;
                    borderFabricObject.actor = border;
                    border.shape = newImageShape;

                    SceneManager.canvas.add(borderFabricObject);
                    borderFabricObject.center();

                    needUpdate = true;
                }

                // Update border size
                if (needSizeUpdate) {
                    const clipPath = borderFabricObject.clipPath;
                    switch (newImageShape) {
                        case 'circle':
                            borderFabricObject.set({
                                radius: newImageSize.radius
                            });
                            clipPath.setRadius(
                                Math.max(
                                    newImageSize.radius - borderWidth + 1,
                                    0
                                )
                            );
                            break;
                        case 'rectangle':
                            borderFabricObject.set(newImageSize);
                            clipPath.set({
                                width: newImageSize.width - borderWidth * 2 + 2,
                                height:
                                    newImageSize.height - borderWidth * 2 + 2
                            });
                            break;
                        default:
                            throw new Error(`Unhandled type: ${border.shape}`);
                    }

                    needUpdate = true;
                }

                if (needUpdate) {
                    // Can't update the border editor panel group, so clear selection to hide it
                    SceneManager.clearSelectedActors();
                    SceneManager.requestRenderAll();
                }
            }
        );
        const handleActorRemove = ({ detail: actor }) => {
            if (actor === border) {
                unsubscribe();
                document.removeEventListener(
                    'scene:actor:removed',
                    handleActorRemove
                );
            }
        };
        document.addEventListener('scene:actor:removed', handleActorRemove);
    },
    _centerOnAdd: function(actor) {
        actor.fabricObject.on('added', () => actor.fabricObject.center());
    }
};

export default ActorFactory;
