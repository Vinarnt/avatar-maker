import React, { useState } from 'react';
import styled from 'styled-components';
import Dropzone from 'react-dropzone';
import Drawer from 'react-motion-drawer';

import EditorPanel from '@components/Editor/EditorPanel/EditorPanel';
import SceneManager from '@services/SceneManager';
import ActorFactory from '@factories/ActorFactory';
import Header from '@components/Header/Header';
import Workplace from '@components/Workplace/Workplace';
import { Desktop, MobileOrTablet } from '@utils/mediaQuery';

function App () {
    const [editing, setEditing] = useState(false);

    return (
        <AppWrapper>
            <Header />
            <main className="main">
                {editing && (
                    <>
                        <Workplace />

                        <Desktop>
                            <EditorPanel />
                        </Desktop>

                        <MobileOrTablet>
                            <Drawer
                                right="true"
                                handleWidth={75}
                                panTolerance={5}
                            >
                                <EditorPanel />
                            </Drawer>
                        </MobileOrTablet>
                    </>
                )}
                {!editing && (
                    <Dropzone onDrop={handleDrop} multiple={false}>
                        {({ getRootProps, getInputProps }) => (
                            <section>
                                <div
                                    {...getRootProps({
                                        className: 'dropzone'
                                    })}
                                >
                                    <input {...getInputProps()} />
                                    <span>
                                        Drop your image here, or click to select
                                        file
                                    </span>
                                </div>
                            </section>
                        )}
                    </Dropzone>
                )}
            </main>
        </AppWrapper>
    );

    function handleDrop (acceptedFiles) {
        if (acceptedFiles.length > 0) {
            setEditing(true);

            const file = acceptedFiles[0];
            SceneManager.addActor(ActorFactory.createImage(file.name, file));
        }
    }
}

const AppWrapper = styled.div`
    display: grid;
    grid-template-rows: 45px auto;
    height: 100%;

    .main {
        position: relative;
        background-color: ${({ theme }) =>
            theme.colors.background.primaryLight1};
        overflow: hidden;

        @media (min-width: ${({ theme }) => theme.breakpoints.md}px) {
            display: grid;
            grid-template-columns: auto 300px;
        }

        .dropzone {
            position: absolute;
            top: 50%;
            left: 50%;
            transform: translate(-50%, -50%);
            width: 200px;
            height: 200px;
            border: azure dashed 1px;
            border-radius: 100%;
            background-color: ${({ theme }) =>
                theme.colors.background.primaryLight2};
            color: azure;

            span {
                position: absolute;
                top: 50%;
                transform: translateY(-50%);
                padding: 20px;
                text-align: center;
            }
        }
    }
`;

export default App;
