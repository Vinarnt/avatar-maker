const session = {
    state: {
        image: null
    },
    reducers: {
        setImage: (state, payload) => {
            return {
                ...state,
                image: payload
            };
        }
    }
};

export default session;
