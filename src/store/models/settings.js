import settingIdentifiers from '@constants/settings';

const settings = {
    state: {
        [settingIdentifiers.general.image.shape]: 'circle',
        [settingIdentifiers.general.image.size]: { radius: 100 }
    },
    reducers: {
        setSettings: (rootState, settings) => ({ ...settings })
    }
};

export default settings;
