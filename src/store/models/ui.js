const ui = {
    state: {
        collapsibleStates: new Map([
            ['Transform', true],
            ['Display', true],
            ['Stroke', true],
            ['Shape', true],
            ['Text', true],
            ['Image', true],
            ['Border', true]
        ])
    },
    reducers: {
        toggleCollapsibleState: (rootState, key) => {
            const states = new Map(rootState.collapsibleStates);
            states.set(key, !states.get(key));
            return { ...rootState, collapsibleStates: states };
        }
    },
    selectors: (slice, createSelector, hasProps) => ({
        getCollapsibleState: hasProps((models, key) =>
            slice(ui => ui.collapsibleStates.get(key))
        )
    })
};

export default ui;
