import _ from 'lodash';
import { sortByZIndex } from '@utils/actor';

const scene = {
    state: {
        isInitialized: false,
        actors: [],
        selectedActors: []
    },
    reducers: {
        setInitialized: (state, isInitialized) => ({ ...state, isInitialized }),
        addActor: (state, actor) => {
            return {
                ...state,
                actors: [...state.actors, actor].sort(sortByZIndex)
            };
        },
        removeActor: (state, actor) => {
            _.pull(state.actors, actor);
            _.pull(state.selectedActors, actor);
            return {
                ...state
            };
        },
        updateActors: state => {
            return {
                ...state,
                actors: state.actors.sort(sortByZIndex)
            };
        },
        addSelectedActor: (state, actor) => {
            return {
                ...state,
                selectedActors: [...state.selectedActors, actor]
            };
        },
        addSelectedActors: (state, actor) => {
            return {
                ...state,
                selectedActors: [...state.selectedActors, ...actor]
            };
        },
        removeSelectedActor: (state, actor) => {
            return {
                ...state,
                selectedActors: state.selectedActors.filter(
                    selectedActor => selectedActor !== actor
                )
            };
        },
        removeSelectedActors: (state, actor) => {
            return {
                ...state,
                selectedActors: _.difference(state.selectedActors, actor)
            };
        },
        clearSelectedActors: state => {
            return {
                ...state,
                selectedActors: []
            };
        }
    },
    selectors: (slice, createSelector, hasProps) => ({
        isSelected: hasProps((models, actor) =>
            slice(state => state.selectedActors.includes(actor))
        ),
        selectionCount: () => slice(state => state.selectedActors.length)
    })
};

export default scene;
