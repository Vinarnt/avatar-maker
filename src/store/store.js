import { init } from '@rematch/core';
import selectPlugin from '@rematch/select';
import session from './models/session';
import scene from './models/scene';
import ui from '@store/models/ui';
import settings from '@store/models/settings';

const store = init({
    models: { session, scene, ui, settings },
    plugins: [selectPlugin()]
});

export default store;
