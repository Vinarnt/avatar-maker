import store from '@store/store';

const { getState } = store;
const storeSubscribe = store.subscribe;

/**
 * Subscribe to the store for a property change
 *
 * @param model the model to subscribe to
 * @param property the model state property to subscribe to
 * @param callback function to call when the property has been changed
 * @return the unsubscribe function
 */
function subscribe(model, property, callback) {
    let lastValue = _getStateValue(model, property);
    return storeSubscribe(() => {
        let currentValue = _getStateValue(model, property);
        if (currentValue !== lastValue) {
            const _lastValue = lastValue;
            // Update lastValue before calling the subscriber to avoid
            // concurrent store update calls from the subscriber with
            // out of date values that would rerun while it shouldn't
            lastValue = currentValue;
            callback(_lastValue, currentValue);
        }
    });
}

function _getStateValue(model, property) {
    const modelState = getState()[model];
    return property ? modelState[property] : modelState;
}

export default subscribe;
