const settings = {
    general: {
        image: {
            shape: 'general.image.shape',
            size: 'general.image.size'
        }
    }
};

export default settings;
