export function _initFabricObject(canvas, object) {
    Object.assign(object, {
        borderColor: 'blue',
        cornerColor: 'green',
        cornerStyle: 'circle',
        originX: 'center',
        originY: 'center',
        transparentCorners: false,
        borderScaleFactor: 2,
        centeredRotation: true
    });
    object.setNonUniScaleControlsVisibility(canvas.uniformScaling);
}

export function sortByZIndex(actorA, actorB) {
    const zindexA = actorA.fabricObject?.getZIndex() || -1;
    const zindexB = actorB.fabricObject?.getZIndex() || -1;

    if (zindexA > zindexB) return -1;
    else return Number(zindexA < zindexB);
}
