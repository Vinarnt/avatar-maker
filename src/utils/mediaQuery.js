import { useMediaQuery } from 'react-responsive';

import theme from '@themes/default';

export const Desktop = ({ children }) => {
    const isDesktop = useMediaQuery({ minWidth: theme.breakpoints.md });
    return isDesktop ? children : null;
};
export const Tablet = ({ children }) => {
    const isTablet = useMediaQuery({
        minWidth: theme.breakpoints.sm,
        maxWidth: theme.breakpoints.md - 1
    });
    return isTablet ? children : null;
};
export const Mobile = ({ children }) => {
    const isMobile = useMediaQuery({ maxWidth: theme.breakpoints.sm - 1 });
    return isMobile ? children : null;
};

export const MobileOrTablet = ({children}) => {
    const isTablet = useMediaQuery({
        minWidth: theme.breakpoints.sm,
        maxWidth: theme.breakpoints.md - 1
    });
    const isMobile = useMediaQuery({ maxWidth: theme.breakpoints.sm - 1 });

    return (isTablet || isMobile) ? children : null;
};
