import { readableColor } from 'polished';

export function getCSSColorDeclaration(theme, bgColor) {
    const textColor = readableColor(
        bgColor,
        theme.colors.text.dark,
        theme.colors.text.light
    );

    return `
        background-color: ${bgColor};
        color: ${textColor};
    `;
}
