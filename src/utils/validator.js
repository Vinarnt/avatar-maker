export function isImageUrlValid(url) {
    return url.match(/https?:\/\/.*\/.*\.(?:gif|jpg|jpeg|png|svg)/);
}
