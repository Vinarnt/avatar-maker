export function countDigits(number) {
    const numberStr = String(number);
    const pointIndex = numberStr.indexOf('.');
    const numberLength = numberStr.length;
    return pointIndex >= 1 ? numberLength - pointIndex - 1 : 0;
}
