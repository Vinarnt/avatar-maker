(function() {
    if (typeof window.CustomEvent === 'function') return false;

    function CustomEvent(event, params) {
        params = params || { bubbles: false, cancelable: false, detail: null };
        const bakingEvent = document.createEvent('CustomEvent');
        bakingEvent.initCustomEvent(
            event,
            params.bubbles,
            params.cancelable,
            params.detail
        );
        return bakingEvent;
    }

    window.CustomEvent = CustomEvent;
})();
