import { darken, lighten } from 'polished';

const primary = '#2d3138';

const theme = {
    spacings: {
        xs: '0.3em',
        sm: '0.5em',
        md: '1em',
        lg: '1.3em',
        xl: '1.6em',
        xxl: '2.25em'
    },
    sizes: {
        xs: '0.6em',
        sm: '0.8em',
        md: '1em',
        lg: '1.2em',
        xl: '1.35em',
        xxl: '1.5em',
        canvas: {
            guides: {
                aligning: 1,
                localCentering: 2,
                globalCentering: 2
            }
        }
    },
    breakpoints: {
        xs: 0,
        sm: 600,
        md: 960,
        lg: 1280,
        xl: 1920
    },
    colors: {
        background: {
            primaryLight6: lighten(0.3, primary),
            primaryLight5: lighten(0.25, primary),
            primaryLight4: lighten(0.2, primary),
            primaryLight3: lighten(0.15, primary),
            primaryLight2: lighten(0.1, primary),
            primaryLight1: lighten(0.05, primary),
            primary: primary,
            primaryDark1: darken(0.05, primary),
            primaryDark2: darken(0.1, primary)
        },
        foreground: {
            primary: 'rgb(83, 98, 209)',
            info: 'rgb(9, 116, 148)',
            success: 'rgb(0, 131, 61)',
            warning: 'rgb(162, 33, 3)',
            danger: 'rgb(204, 51, 0)',
            neutral: 'transparent',
            active: '#5f7187'
        },
        text: {
            light: '#ececec',
            dark: '#2d2d2d'
        },
        canvas: {
            guides: {
                aligning: 'rgb(0,255,0)',
                localCentering: 'rgb(0,0,255)',
                globalCentering: 'rgb(255,0,0)'
            }
        }
    }
};

export default theme;
