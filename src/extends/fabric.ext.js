import { fabric } from 'fabric';

fabric.Object.prototype.getZIndex = function() {
    return this.canvas.getObjects().indexOf(this);
};

fabric.Image.prototype.scaleToFit = function(newWidth, newHeight) {
    this.scale(
        fabric.util.findScaleToFit(this.getOriginalSize(), {
            width: newWidth,
            height: newHeight
        })
    );

    return this;
};
fabric.Image.prototype.scaleToCover = function(newWidth, newHeight) {
    this.scale(
        fabric.util.findScaleToCover(this.getOriginalSize(), {
            width: newWidth,
            height: newHeight
        })
    );

    return this;
};

fabric.Object.prototype.centeredScaleTo = function(scaleX, scaleY) {
    const originalWidth = this.width;
    const originalHeight = this.height;
    const currentWidth = this.getScaledWidth();
    const currentHeight = this.getScaledHeight();
    const newWidth = originalWidth * scaleX;
    const newHeight = originalHeight * scaleY;
    const widthDiff = newWidth - currentWidth;
    const heightDiff = newHeight - currentHeight;

    this.set({
        left: this.left - widthDiff * 0.5,
        top: this.top - heightDiff * 0.5,
        scaleX: scaleX,
        scaleY: scaleY
    }).setCoords();
};

fabric.Object.prototype.centeredScaleToHeight = function(
    height,
    absolute = false
) {
    const boundingRectFactor =
        this.getBoundingRect(absolute).height / this.getScaledHeight();
    const scale = height / this.height / boundingRectFactor;
    this.centeredScaleTo(scale, scale);
};

fabric.ActiveSelection.prototype.fixPosition = function() {
    if(this.originX === 'center' && this.originY === 'center') {
        this.set({
            left: this.left + this.width * 0.5,
            top: this.top + this.height * 0.5
        });
        this.setCoords();
    }
};

fabric.Object.prototype.setNonUniScaleControlsVisibility = function(
    visibility
) {
    this.setControlVisible('ml', visibility);
    this.setControlVisible('mt', visibility);
    this.setControlVisible('mr', visibility);
    this.setControlVisible('mb', visibility);
};

function _extendsFabricStackingFunction(protoFuncName) {
    fabric.StaticCanvas.prototype[protoFuncName] = (function(oldProtoFunc) {
        return function(target, ...args) {
            oldProtoFunc.call(this, target, ...args);
            this.fire('object:depth:modified', { target });

            return this;
        };
    })(fabric.StaticCanvas.prototype[protoFuncName]);
}

_extendsFabricStackingFunction('sendToBack');
_extendsFabricStackingFunction('sendBackwards');
_extendsFabricStackingFunction('bringToFront');
_extendsFabricStackingFunction('bringForward');
_extendsFabricStackingFunction('moveTo');

fabric.Canvas.prototype.projectViewport = function(x, y) {
    const canvasPos = this.getElement().getBoundingClientRect();
    const viewportBoundaries = this.calcViewportBoundaries();

    return new fabric.Point(
        x - canvasPos.left + viewportBoundaries.tl.x,
        y - canvasPos.top + viewportBoundaries.tl.y
    );
};

fabric.Color.prototype.toRgbaObject = function() {
    const _source = this.getSource();
    return {
        r: _source[0],
        g: _source[1],
        b: _source[2],
        a: _source[3]
    };
};

fabric.Color.fromRgbaObject = function(object) {
    const color = new fabric.Color();
    color.setSource([object.r, object.g, object.b, object.a]);
    return color;
};
