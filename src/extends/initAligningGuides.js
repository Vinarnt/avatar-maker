/**
 * Should objects be aligned by a bounding box?
 */
import { fabric } from 'fabric';

import theme from '@themes/default';

function initAligningGuidelines(canvas) {
    let ctx = canvas.getSelectionContext(),
        aligningLineOffset = 5,
        aligningLineMargin = 10,
        aligningLineColor = theme.colors.canvas.guides.aligning,
        aligningLineWidth = theme.sizes.canvas.guides.aligning,
        localCenteringLineColor = theme.colors.canvas.guides.localCentering,
        localCenteringLineWidth = theme.sizes.canvas.guides.localCentering,
        globalCenteringLineColor = theme.colors.canvas.guides.globalCentering,
        globalCenteringLineWidth = theme.sizes.canvas.guides.globalCentering,
        viewportTransform,
        zoom = 1;

    let verticalLines = [],
        horizontalLines = [];

    function drawVerticalLine(coords, color, width) {
        drawLine(
            coords.x + 0.5,
            coords.y1 > coords.y2 ? coords.y2 : coords.y1,
            coords.x + 0.5,
            coords.y2 > coords.y1 ? coords.y2 : coords.y1,
            color,
            width
        );
    }

    function drawHorizontalLine(coords, color, width) {
        drawLine(
            coords.x1 > coords.x2 ? coords.x2 : coords.x1,
            coords.y + 0.5,
            coords.x2 > coords.x1 ? coords.x2 : coords.x1,
            coords.y + 0.5,
            color,
            width
        );
    }

    function drawLine(x1, y1, x2, y2, color, width = 1) {
        ctx.save();
        ctx.lineWidth = width;
        ctx.strokeStyle = color;
        ctx.beginPath();
        ctx.moveTo(
            x1 * zoom + viewportTransform[4],
            y1 * zoom + viewportTransform[5]
        );
        ctx.lineTo(
            x2 * zoom + viewportTransform[4],
            y2 * zoom + viewportTransform[5]
        );
        ctx.stroke();
        ctx.restore();
    }

    function isInRange(value1, value2) {
        value1 = Math.round(value1);
        value2 = Math.round(value2);
        for (
            let i = value1 - aligningLineMargin,
                len = value1 + aligningLineMargin;
            i <= len;
            i++
        ) {
            if (i === value2) {
                return true;
            }
        }
        return false;
    }

    function handleMoving(e) {
        let activeObject = e.target,
            canvasObjects = canvas.getObjects(),
            activeObjectCenter = activeObject.getCenterPoint(),
            activeObjectLeft = activeObjectCenter.x,
            activeObjectTop = activeObjectCenter.y,
            activeObjectBoundingRect = activeObject.getBoundingRect(),
            activeObjectHeight =
                activeObjectBoundingRect.height / viewportTransform[3],
            activeObjectWidth =
                activeObjectBoundingRect.width / viewportTransform[0],
            horizontalInTheRange = false,
            verticalInTheRange = false,
            transform = canvas._currentTransform,
            x = activeObjectLeft,
            y = activeObjectTop;

        if (!transform) return;

        // It should be trivial to DRY this up by encapsulating (repeating) creation of x1, x2, y1, and y2 into functions,
        // but we're not doing it here for perf. reasons -- as this a function that's invoked on every mouse move

        for (let i = canvasObjects.length; i--; ) {
            if (canvasObjects[i] === activeObject) continue;

            let objectCenter = canvasObjects[i].getCenterPoint(),
                objectLeft = objectCenter.x,
                objectTop = objectCenter.y,
                objectBoundingRect = canvasObjects[i].getBoundingRect(),
                objectHeight = objectBoundingRect.height / viewportTransform[3],
                objectWidth = objectBoundingRect.width / viewportTransform[0];

            // snap by the horizontal center line
            if (isInRange(objectLeft, activeObjectLeft)) {
                verticalInTheRange = true;
                verticalLines.push({
                    coords: {
                        x: objectLeft,
                        y1: 0,
                        y2: canvas.getHeight()
                    },
                    color: localCenteringLineColor,
                    width: localCenteringLineWidth
                });
                x = objectLeft;
            }

            // snap by the left edge
            if (
                isInRange(
                    objectLeft - objectWidth / 2,
                    activeObjectLeft - activeObjectWidth / 2
                )
            ) {
                verticalInTheRange = true;
                verticalLines.push({
                    coords: {
                        x: objectLeft - objectWidth / 2,
                        y1: Math.min(
                            activeObjectTop -
                                activeObjectHeight / 2 -
                                aligningLineOffset,
                            objectTop - objectHeight / 2
                        ),
                        y2: Math.max(
                            activeObjectTop > objectTop
                                ? activeObjectTop +
                                      activeObjectHeight / 2 +
                                      aligningLineOffset
                                : activeObjectTop -
                                      activeObjectHeight / 2 -
                                      aligningLineOffset,
                            objectTop + objectHeight / 2
                        )
                    },
                    color: aligningLineColor,
                    width: aligningLineWidth
                });
                x = objectLeft - objectWidth / 2 + activeObjectWidth / 2;
            } else if (
                isInRange(
                    objectLeft + objectWidth / 2,
                    activeObjectLeft - activeObjectWidth / 2
                )
            ) {
                verticalInTheRange = true;
                verticalLines.push({
                    coords: {
                        x: objectLeft + objectWidth / 2,
                        y1: Math.min(
                            activeObjectTop -
                                activeObjectHeight / 2 -
                                aligningLineOffset,
                            objectTop - objectHeight / 2
                        ),
                        y2: Math.max(
                            activeObjectTop > objectTop
                                ? activeObjectTop +
                                      activeObjectHeight / 2 +
                                      aligningLineOffset
                                : activeObjectTop -
                                      activeObjectHeight / 2 -
                                      aligningLineOffset,
                            objectTop + objectHeight / 2
                        )
                    },
                    color: aligningLineColor,
                    width: aligningLineWidth
                });
                x = objectLeft + objectWidth / 2 + activeObjectWidth / 2;
            }

            // snap by the right edge
            if (
                isInRange(
                    objectLeft + objectWidth / 2,
                    activeObjectLeft + activeObjectWidth / 2
                )
            ) {
                verticalInTheRange = true;
                verticalLines.push({
                    coords: {
                        x: objectLeft + objectWidth / 2,
                        y1: Math.min(
                            activeObjectTop -
                                activeObjectHeight / 2 -
                                aligningLineOffset,
                            objectTop - objectHeight / 2
                        ),
                        y2: Math.max(
                            activeObjectTop > objectTop
                                ? activeObjectTop +
                                      activeObjectHeight / 2 +
                                      aligningLineOffset
                                : activeObjectTop -
                                      activeObjectHeight / 2 -
                                      aligningLineOffset,
                            objectTop + objectHeight / 2
                        )
                    },
                    color: aligningLineColor,
                    width: aligningLineWidth
                });

                x = objectLeft + objectWidth / 2 - activeObjectWidth / 2;
            } else if (
                isInRange(
                    objectLeft - objectWidth / 2,
                    activeObjectLeft + activeObjectWidth / 2
                )
            ) {
                verticalInTheRange = true;
                verticalLines.push({
                    coords: {
                        x: objectLeft - objectWidth / 2,
                        y1: Math.min(
                            activeObjectTop -
                                activeObjectHeight / 2 -
                                aligningLineOffset,
                            objectTop - objectHeight / 2
                        ),
                        y2: Math.max(
                            activeObjectTop > objectTop
                                ? activeObjectTop +
                                      activeObjectHeight / 2 +
                                      aligningLineOffset
                                : activeObjectTop -
                                      activeObjectHeight / 2 -
                                      aligningLineOffset,
                            objectTop + objectHeight / 2
                        )
                    },
                    color: aligningLineColor,
                    width: aligningLineWidth
                });
                x = objectLeft - objectWidth / 2 - activeObjectWidth / 2;
            }

            // snap by the vertical center line
            if (isInRange(objectTop, activeObjectTop)) {
                horizontalInTheRange = true;
                horizontalLines.push({
                    coords: {
                        y: objectTop,
                        x1: 0,
                        x2: canvas.getWidth()
                    },
                    color: localCenteringLineColor,
                    width: localCenteringLineWidth
                });
                y = objectTop;
            }

            // snap by the top edge
            if (
                isInRange(
                    objectTop - objectHeight / 2,
                    activeObjectTop - activeObjectHeight / 2
                )
            ) {
                horizontalInTheRange = true;
                horizontalLines.push({
                    coords: {
                        y: objectTop - objectHeight / 2,
                        x1: Math.min(
                            activeObjectLeft -
                                activeObjectWidth / 2 -
                                aligningLineOffset,
                            objectLeft - objectWidth / 2
                        ),
                        x2: Math.max(
                            activeObjectLeft > objectLeft
                                ? activeObjectLeft +
                                      activeObjectWidth / 2 +
                                      aligningLineOffset
                                : activeObjectLeft -
                                      activeObjectWidth / 2 -
                                      aligningLineOffset,
                            objectLeft + objectWidth / 2
                        )
                    },
                    color: aligningLineColor,
                    width: aligningLineWidth
                });
                y = objectTop - objectHeight / 2 + activeObjectHeight / 2;
            } else if (
                isInRange(
                    objectTop + objectHeight / 2,
                    activeObjectTop - activeObjectHeight / 2
                )
            ) {
                horizontalInTheRange = true;
                horizontalLines.push({
                    coords: {
                        y: objectTop + objectHeight / 2,
                        x1: Math.min(
                            activeObjectLeft -
                                activeObjectWidth / 2 -
                                aligningLineOffset,
                            objectLeft - objectWidth / 2
                        ),
                        x2: Math.max(
                            activeObjectLeft > objectLeft
                                ? activeObjectLeft +
                                      activeObjectWidth / 2 +
                                      aligningLineOffset
                                : activeObjectLeft -
                                      activeObjectWidth / 2 -
                                      aligningLineOffset,
                            objectLeft + objectWidth / 2
                        )
                    },
                    color: aligningLineColor,
                    width: aligningLineWidth
                });
                y = objectTop + objectHeight / 2 + activeObjectHeight / 2;
            }

            // snap by the bottom edge
            if (
                isInRange(
                    objectTop + objectHeight / 2,
                    activeObjectTop + activeObjectHeight / 2
                )
            ) {
                horizontalInTheRange = true;
                horizontalLines.push({
                    coords: {
                        y: objectTop + objectHeight / 2,
                        x1: Math.min(
                            activeObjectLeft -
                                activeObjectWidth / 2 -
                                aligningLineOffset,
                            objectLeft - objectWidth / 2
                        ),
                        x2: Math.max(
                            activeObjectLeft > objectLeft
                                ? activeObjectLeft +
                                      activeObjectWidth / 2 +
                                      aligningLineOffset
                                : activeObjectLeft -
                                      activeObjectWidth / 2 -
                                      aligningLineOffset,
                            objectLeft + objectWidth / 2
                        )
                    },
                    color: aligningLineColor,
                    width: aligningLineWidth
                });
                y = objectTop + objectHeight / 2 - activeObjectHeight / 2;
            } else if (
                isInRange(
                    objectTop - objectHeight / 2,
                    activeObjectTop + activeObjectHeight / 2
                )
            ) {
                horizontalInTheRange = true;
                horizontalLines.push({
                    coords: {
                        y: objectTop - objectHeight / 2,
                        x1: Math.min(
                            activeObjectLeft -
                                activeObjectWidth / 2 -
                                aligningLineOffset,
                            objectLeft - objectWidth / 2
                        ),
                        x2: Math.max(
                            activeObjectLeft > objectLeft
                                ? activeObjectLeft +
                                      activeObjectWidth / 2 +
                                      aligningLineOffset
                                : activeObjectLeft -
                                      activeObjectWidth / 2 -
                                      aligningLineOffset,
                            objectLeft + objectWidth / 2
                        )
                    },
                    color: aligningLineColor,
                    width: aligningLineWidth
                });
                y = objectTop - objectHeight / 2 - activeObjectHeight / 2;
            }
        }

        // snap by the horizontal center line
        const canvasWidth = canvas.getWidth();
        const canvasHalfWidth = canvasWidth / 2;
        const canvasHeight = canvas.getHeight();
        const canvasHalfHeight = canvasHeight / 2;
        if (isInRange(canvasHalfWidth, activeObjectLeft)) {
            verticalInTheRange = true;
            verticalLines.push({
                coords: {
                    x: canvasHalfWidth,
                    y1: 0,
                    y2: canvasHeight
                },
                color: globalCenteringLineColor,
                width: globalCenteringLineWidth
            });
            x = canvasHalfWidth;
        }
        // snap by the vertical center line
        if (isInRange(canvasHalfHeight, activeObjectTop)) {
            horizontalInTheRange = true;
            horizontalLines.push({
                coords: {
                    y: canvasHalfHeight,
                    x1: 0,
                    x2: canvasWidth
                },
                color: globalCenteringLineColor,
                width: globalCenteringLineWidth
            });
            y = canvasHalfHeight;
        }

        activeObject.setPositionByOrigin(
            new fabric.Point(x, y),
            'center',
            'center'
        );

        if (!horizontalInTheRange) {
            horizontalLines.length = 0;
        }

        if (!verticalInTheRange) {
            verticalLines.length = 0;
        }
    }

    canvas.on('mouse:down', function() {
        viewportTransform = canvas.viewportTransform;
        zoom = canvas.getZoom();
    });

    canvas.on('object:moving', handleMoving);

    canvas.on('before:render', function() {
        if (canvas.contextTop) canvas.clearContext(canvas.contextTop);
    });

    canvas.on('after:render', function() {
        for (let i = verticalLines.length; i--; ) {
            const line = verticalLines[i];
            drawVerticalLine(line.coords, line.color, line.width);
        }
        for (let i = horizontalLines.length; i--; ) {
            const line = horizontalLines[i];
            drawHorizontalLine(line.coords, line.color, line.width);
        }

        verticalLines.length = horizontalLines.length = 0;
    });

    canvas.on('mouse:up', function() {
        verticalLines.length = horizontalLines.length = 0;
        canvas.renderAll();
    });
}

export default initAligningGuidelines;
