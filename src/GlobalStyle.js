import { createGlobalStyle } from 'styled-components';

import { getCSSColorDeclaration } from '@utils/color';
import DropDownArrowIcon from '@assets/icons/arrow_drop_down-24px.svg';

const GlobalStyle = createGlobalStyle`
    * {
        font-family: 'Helvetica Neue', sans-serif;
        box-sizing: border-box;
        scrollbar-width: auto;
        scrollbar-color: 
            ${({ theme }) => theme.colors.background.primaryLight1} 
            ${({ theme }) => theme.colors.background.primaryDark1};
    }

    html,
    body,
    #root {
        height: 100%;
    }

    body {
        margin: 0;
        overflow: hidden;
    }

    h1 {
        font-size: 1.25rem;
    }
    h2 {
        font-size: 1.1rem;
    }
    h3 {
        font-size: 1rem;
    }

    svg path:first-child {
        fill: white;
    }

    .hidden {
        display: none;
    }

    .underline {
        text-decoration: underline;
    }

    .overline {
        text-decoration: overline;
    }

    .line-through {
        text-decoration: line-through;
    }

    .text-center {
        text-align: center;
    }
    
    ::-webkit-scrollbar {
        width: ${({ theme }) => theme.sizes.sm};
    }
    
    ::-webkit-scrollbar-track {
        background-color: ${({ theme }) =>
            theme.colors.background.primaryDark1};
    }
    
    ::-webkit-scrollbar-thumb {
        background-color: ${({ theme }) =>
            theme.colors.background.primaryLight1};
        border: 1px solid ${({ theme }) =>
            theme.colors.background.primaryDark1};
    }
    
    ::-webkit-scrollbar-thumb:hover {
        background-color: ${({ theme }) =>
            theme.colors.background.primaryLight2};
    }
    
    ::-webkit-scrollbar-thumb:active {
        background-color: ${({ theme }) =>
            theme.colors.background.primaryLight3};
    }
    
    ::-webkit-scrollbar-track-piece {
        background: transparent none;
        border: calc(${({ theme }) => theme.sizes.sm} / 3) solid transparent;
        box-shadow: inset calc(${({ theme }) =>
            theme.sizes.sm} / 3) 1px 0 0 ${({ theme }) =>
    theme.colors.background.primaryLight1};
    }
    
    ::-webkit-scrollbar-track-piece:horizontal {
        border-right-width: 4px;
        border-bottom-width: 4px;
    }

    input:not([type]), input[type="text"], input[type="number"] {
        padding: 0 ${({ theme }) => theme.spacings.xs};
    }
  
    input, select:not([multiple]) {
        height: ${({ theme }) => theme.sizes.xxl};
        ${props =>
            getCSSColorDeclaration(
                props.theme,
                props.theme.colors.background.primary
            )};
        border: 1px solid ${({ theme }) =>
            theme.colors.background.primaryLight5};
        
        &:hover, &:focus {
            filter: brightness(80%);
        }
        
        &:disabled {
            filter: grayscale(0.5);
            opacity: 0.5;
            pointer-events: none;
      }
    }
    
    input[type="checkbox"] {
      cursor: pointer;
      appearance: none;
      outline: 0;
      height: 16px;
      width: 16px;
      margin: 0;
      
      &:checked:after {
        content: '';
        display: inline-block;
        background-color: ${({ theme }) =>
            theme.colors.background.primaryLight6};
        margin: 2px;
        width: 10px;
        height: 10px;
      }
    }
    
    select {
        appearance: none;
        background: url(${DropDownArrowIcon}) no-repeat right;
        ${props =>
            getCSSColorDeclaration(
                props.theme,
                props.theme.colors.background.primary
            )};
    }

    .react-contexify {
        z-index: 10001;
    }
`;

export default GlobalStyle;
