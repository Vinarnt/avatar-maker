import React from 'react';
import styled from 'styled-components';

import { getCSSColorDeclaration } from '../../utils/color';
import GitlabButton from '@components/Button/GitlabButton';

function Header() {
    return (
        <Wrapper>
            <LeftContainer>
                <h1>Avatar Maker</h1>
            </LeftContainer>
            <CenterContainer />
            <RightContainer>
                <GitlabButton className="gitlab-btn" />
            </RightContainer>
        </Wrapper>
    );
}

const Wrapper = styled.header`
    display: flex;
    ${({ theme }) =>
        getCSSColorDeclaration(theme, theme.colors.background.primary)};
    align-items: center;
    padding: 0 ${({ theme }) => theme.spacings.sm};

    h1 {
        user-select: none;
    }
`;
const LeftContainer = styled.div`
    display: flex;
    align-items: center;
    justify-content: start;
    flex-grow: 1;
`;
const CenterContainer = styled.div`
    display: flex;
    flex-grow: 1;
    justify-content: center;
    align-items: center;
    margin: 0 ${({ theme }) => theme.spacings.sm};
`;
const RightContainer = styled.div`
    display: flex;
    flex-direction: row-reverse;
    align-items: center;
    flex-grow: 1;
`;

export default Header;
