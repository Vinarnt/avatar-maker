import React from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components';

import SmallNumberInput from '@components/Setting/Group/SmallNumberInput';
import SubLabel from '@components/Setting/SubLabel';

function RadiusInput({ radius, onChange }) {
    return (
        <Wrapper>
            <SubLabel htmlFor="radius-input">Radius</SubLabel>
            <Input
                id="radius-input"
                value={radius}
                min={10}
                onChange={onChange}
            />
        </Wrapper>
    );
}
RadiusInput.propTypes = {
    radius: PropTypes.number.isRequired,
    onChange: PropTypes.func.isRequired
};

const Input = styled(SmallNumberInput)``;
const Wrapper = styled.div`
    display: inline-flex;
    flex-direction: column;

    & > label {
        margin-bottom: ${({ theme }) => theme.spacings.xs};
        text-align: center;
    }
`;

export default RadiusInput;
