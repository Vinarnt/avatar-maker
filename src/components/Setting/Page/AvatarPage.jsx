import React from 'react';
import PropTypes from 'prop-types';

import settingIdentifiers from '@constants/settings';
import ClipPathGroup from '@components/Setting/Group/ClipPathGroup';

function AvatarPage({ settings, onChange }) {
    return (
        <ClipPathGroup
            shape={settings[settingIdentifiers.general.image.shape]}
            size={settings[settingIdentifiers.general.image.size]}
            onChange={onChange}
        />
    );
}
AvatarPage.propTypes = {
    settings: PropTypes.object.isRequired,
    onChange: PropTypes.func.isRequired
};

export default AvatarPage;
