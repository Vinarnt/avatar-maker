import React, { useState } from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components';
import { FaLock, FaLockOpen } from 'react-icons/fa';

import SmallNumberInput from '@components/Setting/Group/SmallNumberInput';
import Button from '@components/Button/Button';
import SubLabel from '@components/Setting/SubLabel';

function SizeInput({ width, height, onChange }) {
    const [keepAspectRatio, setKeepAspectRatio] = useState(true);

    function handleWidthChange(newWidth) {
        let newHeight = height;
        if (keepAspectRatio) {
            const aspectRatio = width / height;
            if (newWidth !== width) newHeight = newWidth / aspectRatio;
        }
        onChange(newWidth, newHeight);
    }

    function handleHeightChange(newHeight) {
        let newWidth = width;
        if (keepAspectRatio) {
            const aspectRatio = width / height;
            if (newHeight !== height) newWidth = newHeight * aspectRatio;
        }
        onChange(newWidth, newHeight);
    }

    function handleKeepAspectRatioClick() {
        setKeepAspectRatio(!keepAspectRatio);
    }

    return (
        <Wrapper>
            <WidthLabel htmlFor="width-input">Width</WidthLabel>
            <WidthInput
                id="width-input"
                value={width}
                min={10}
                onChange={handleWidthChange}
            />
            <LockIconWrapper type="button" onClick={handleKeepAspectRatioClick}>
                {keepAspectRatio && <FaLock />}
                {!keepAspectRatio && <FaLockOpen />}
            </LockIconWrapper>
            <HeightLabel htmlFor="height-input">Height</HeightLabel>
            <HeightInput
                id="height-input"
                value={height}
                min={10}
                onChange={handleHeightChange}
            />
        </Wrapper>
    );
}
SizeInput.propTypes = {
    width: PropTypes.number.isRequired,
    height: PropTypes.number.isRequired,
    onChange: PropTypes.func.isRequired
};

const Wrapper = styled.div`
    display: inline-grid;
    grid-template-areas:
        'width-label void height-label'
        'width-input lock-icon height-input';
`;
const WidthLabel = styled(SubLabel)`
    grid-area: width-label;
`;
const HeightLabel = styled(SubLabel)`
    grid-area: height-label;
`;
const LockIconWrapper = styled(Button)`
    display: inline-block;
    grid-area: lock-icon;
    margin: 0 ${({ theme }) => theme.spacings.sm};
    vertical-align: middle;
`;
const WidthInput = styled(SmallNumberInput)`
    grid-area: width-input;
`;
const HeightInput = styled(SmallNumberInput)`
    grid-area: height-input;
`;

export default SizeInput;
