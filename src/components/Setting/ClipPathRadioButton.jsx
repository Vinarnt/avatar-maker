import React from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components';
import classNames from 'classnames';
import { lighten } from 'polished';

import settings from '@constants/settings';
import SubLabel from '@components/Setting/SubLabel';

function ClipPathRadioButton({ checkedShape, onChange }) {
    function isChecked(shape) {
        return shape === checkedShape;
    }

    function handleChange(e) {
        let type;
        switch (e.currentTarget.id) {
            case 'rectangleShapeRadioButton':
                type = 'rectangle';
                break;
            case 'circleShapeRadioButton':
                type = 'circle';
                break;
            default:
        }

        onChange(e, type);
    }

    return (
        <ClipPathContainer>
            <ShapeWrapper
                className={classNames({ checked: isChecked('circle') })}
            >
                <ShapeContainer htmlFor="circleShapeRadioButton">
                    <Circle />
                </ShapeContainer>
                <CustomSubLabel>Rounded</CustomSubLabel>
                <InvisibleInput
                    type="radio"
                    className="form-control"
                    name={settings.general.image.shape}
                    id="circleShapeRadioButton"
                    checked={isChecked('circle')}
                    value="circle"
                    onChange={handleChange}
                />
            </ShapeWrapper>
            <ShapeWrapper
                className={classNames({ checked: isChecked('rectangle') })}
            >
                <ShapeContainer htmlFor="rectangleShapeRadioButton">
                    <Rectangle />
                </ShapeContainer>
                <CustomSubLabel>Rectangle</CustomSubLabel>
                <InvisibleInput
                    type="radio"
                    className="form-control"
                    name={settings.general.image.shape}
                    id="rectangleShapeRadioButton"
                    checked={isChecked('rectangle')}
                    value="rectangle"
                    onChange={handleChange}
                />
            </ShapeWrapper>
        </ClipPathContainer>
    );
}
ClipPathRadioButton.propTypes = {
    checkedShape: PropTypes.oneOf(['circle', 'rectangle']).isRequired,
    onChange: PropTypes.func.isRequired
};

const ClipPathContainer = styled.div`
    display: flex;
    flex-wrap: wrap;
    justify-content: space-evenly;
`;
const ShapeWrapper = styled.div`
    display: inline-flex;
    flex-direction: column;
    padding: 0 ${({ theme }) => theme.spacings.md};
    text-align: center;
`;
const ShapeContainer = styled.label`
    padding: ${({ theme }) => theme.spacings.md};
    border-radius: 5px;
    border: 4px solid transparent;
    background-color: ${({ theme }) => theme.colors.background.primaryLight1};

    :hover,
    ${ShapeWrapper}.checked & {
        border: 4px solid
            ${({ theme }) =>
                lighten(0.3, theme.colors.background.primaryLight5)};
    }
`;
const CustomSubLabel = styled(SubLabel)`
    ${ShapeWrapper}.checked & {
        font-weight: bold;
    }
`;
const Shape = styled.div`
    width: 50px;
    height: 50px;
    background-color: ${({ theme }) =>
        lighten(0.3, theme.colors.background.primaryLight1)};

    ${ShapeWrapper}:hover &,
    ${ShapeWrapper}.checked & {
        background-color: ${({ theme }) =>
            lighten(0.3, theme.colors.background.primaryLight5)};
    }
`;
const Rectangle = styled(Shape)``;
const Circle = styled(Shape)`
    border-radius: 100%;
`;
const InvisibleInput = styled.input`
    display: none;
`;

export default ClipPathRadioButton;
