import styled from 'styled-components';

export default styled.label`
    margin: ${({ theme }) => theme.spacings.xs} 0;
    text-align: center;
`;
