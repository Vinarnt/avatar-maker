import styled from 'styled-components';
import InputNumberEditor from 'react-input-number-editor';

export default styled(InputNumberEditor)`
    width: 100px;
`;
