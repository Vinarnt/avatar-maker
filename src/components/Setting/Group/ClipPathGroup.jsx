import React from 'react';
import PropTypes from 'prop-types';

import ClipPathRadioButton from '@components/Setting/ClipPathRadioButton';
import RadiusInput from '@components/Setting/RadiusInput';
import SizeInput from '@components/Setting/SizeInput';
import settings from '@constants/settings';

function ClipPathGroup({ shape, size, onChange }) {
    function handleTypeChange(e, shape) {
        onChange(settings.general.image.shape, shape);
        onChange(settings.general.image.size, convertSizeForShape(size, shape));
    }

    function handleRadiusChange(radius) {
        onChange(settings.general.image.size, { radius });
    }

    function handleSizeChange(width, height) {
        onChange(settings.general.image.size, { width, height });
    }

    function convertSizeForShape(size, shape) {
        switch (shape) {
            case 'circle':
                if (!size.radius)
                    return { radius: Math.max(size.width, size.height) * 0.5 };
                break;
            case 'rectangle':
                if (size.radius) {
                    const newSize = size.radius * 2;
                    return { width: newSize, height: newSize };
                }
                break;
            default:
                throw new Error('Unexpected behavior');
        }

        return size;
    }

    return (
        <>
            <fieldset className="form-group">
                <label>Avatar shape</label>
                <ClipPathRadioButton
                    checkedShape={shape}
                    onChange={handleTypeChange}
                />
            </fieldset>
            <fieldset>
                <label>Avatar size</label>
                {shape === 'circle' && (
                    <RadiusInput
                        radius={size.radius}
                        onChange={handleRadiusChange}
                    />
                )}
                {shape === 'rectangle' && (
                    <SizeInput
                        width={size.width}
                        height={size.height}
                        onChange={handleSizeChange}
                    />
                )}
            </fieldset>
        </>
    );
}
ClipPathGroup.propTypes = {
    shape: PropTypes.string,
    size: PropTypes.object,
    onChange: PropTypes.func.isRequired
};

export default ClipPathGroup;
