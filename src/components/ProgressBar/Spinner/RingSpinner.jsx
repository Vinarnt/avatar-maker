import React from 'react';
import PropTypes from 'prop-types';
import styled, { keyframes } from 'styled-components';

function RingSpinner({ color, size }) {
    return (
        <Wrapper color={color} size={size}>
            <div />
            <div />
            <div />
            <div />
        </Wrapper>
    );
}
RingSpinner.propTypes = {
    color: PropTypes.string,
    size: PropTypes.string
};
RingSpinner.defaultProps = {
    color: '#a6bcc4',
    size: 'md'
};

const rotate = keyframes`
  0% {
    transform: rotate(0deg);
  }
  100% {
    transform: rotate(360deg);
  }
`;

function getSize() {
    return ({ size }) => {
        switch (size) {
            case 'sm':
                return '1.2rem';
            case 'md':
                return '1.8rem';
            case 'lg':
                return '2.5rem';
            case 'xl':
                return '3.25rem';
            default:
                return size;
        }
    };
}

const Wrapper = styled.div`
  --spinner-color: ${props => props.color};
  --spinner-size: ${getSize()};
  display: inline-block;
  position: relative;
  width: var(--spinner-size);
  height: var(--spinner-size);

  div {
    box-sizing: border-box;
    display: block;
    position: absolute;
    width: 100%;
    height: 100%;
    border: 4px solid var(--spinner-color);
    border-radius: 50%;
    animation: ${rotate} 1.2s cubic-bezier(0.5, 0, 0.5, 1) infinite;
    border-color: var(--spinner-color) transparent transparent transparent;
  }

  div:nth-child(1) {
    animation-delay: -0.45s;
  }

  div:nth-child(2) {
    animation-delay: -0.30s;
  }

  div:nth-child(3) {
    animation-delay: -0.15s;
  }
`;

export default RingSpinner;
