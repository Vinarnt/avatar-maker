import React, { useEffect, useState } from 'react';
import PropTypes from 'prop-types';

import SceneManager from '@services/SceneManager';
import EditorPanelGroup from '../EditorPanelGroup';
import EditorPanelFieldNumberInput from '@components/Editor/EditorPanelGroup/EditorPanelField/EditorPanelFieldNumberInput/EditorPanelFieldNumberInput';
import EditorPanelColorFields from '@components/Editor/EditorPanelGroup/EditorPanelField/EditorPanelColorFields/EditorPanelColorFields';

function BorderPanelGroup({ actor }) {
    const fabricObject = actor.fabricObject;
    const [shape, setShape] = useState(undefined);
    const [maxWidth, setMaxWidth] = useState(0);
    const [width, setWidth] = useState(0);

    useEffect(() => {
        const fabricObject = actor.fabricObject;

        setShape(actor.shape);
        setMaxWidth(Math.max(fabricObject.width / 2, fabricObject.height / 2));
        setWidth(actor.width);
    }, [actor]);

    function handleWidthChange(value) {
        switch (shape) {
            case 'circle':
                fabricObject.clipPath.setRadius(fabricObject.radius - value);
                break;
            case 'rectangle':
                fabricObject.clipPath.set({
                    width: fabricObject.width - value * 2,
                    height: fabricObject.height - value * 2
                });
                break;
            default:
                throw new Error(`Unhandled type: ${shape}`);
        }
        // Force re-rendering of the clipPath
        fabricObject.dirty = true;

        SceneManager.requestRenderAll();
        actor.width = value;
        setWidth(value);
    }

    return (
        <EditorPanelGroup title="Border">
            <EditorPanelFieldNumberInput
                label="Width"
                value={width}
                min={0}
                max={maxWidth}
                slideModifier={0.1}
                onChange={handleWidthChange}
            />
            <EditorPanelColorFields actor={actor} />
        </EditorPanelGroup>
    );
}

BorderPanelGroup.propTypes = {
    actor: PropTypes.object.isRequired
};

export default BorderPanelGroup;
