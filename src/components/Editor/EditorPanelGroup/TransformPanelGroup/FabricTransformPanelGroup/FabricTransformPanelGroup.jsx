import React, { useState, useEffect, useCallback } from 'react';
import PropTypes from 'prop-types';
import _function from 'lodash/function';

import TransformPanelGroup from '../TransformPanelGroup';
import SceneManager from '@services/SceneManager';
import { fabric } from 'fabric';

function FabricTransformPanelGroup(props) {
    const { fabricObject } = props;

    const [positionX, setPositionX] = useState(0);
    const [positionY, setPositionY] = useState(0);
    const [scaleX, setScaleX] = useState(0);
    const [scaleY, setScaleY] = useState(0);
    const [rotation, setRotation] = useState(0);

    const updatePosition = useCallback(_updatePosition, []);

    useEffect(() => {
        setPositionX(fabricObject.left);
        setPositionY(fabricObject.top);
        setScaleX(fabricObject.scaleX);
        setScaleY(fabricObject.scaleY);
        setRotation(fabricObject.angle);

        const onObjectMoving = throttle(event => {
            updatePosition(event.target);
        });
        fabricObject.on('moving', onObjectMoving);
        const onObjectScaling = throttle(({ transform: { target } }) => {
            setScaleX(target.scaleX);
            setScaleY(target.scaleY);
            updatePosition(target);
        });
        fabricObject.on('scaling', onObjectScaling);
        const onObjectRotating = throttle(({ transform: { target } }) => {
            setRotation(fabric.util.toFixed(target.angle, 2));
        });
        fabricObject.on('rotating', onObjectRotating);

        const onSelectionUpdated = () => {
            updatePosition(fabricObject);
            setScaleX(fabricObject.scaleX);
            setScaleY(fabricObject.scaleY);
        };

        if (fabricObject.type === 'activeSelection') {
            document.addEventListener(
                'scene:actor:selected',
                onSelectionUpdated
            );
            document.addEventListener(
                'scene:actor:deselected',
                onSelectionUpdated
            );
            SceneManager.canvas.on('selection:updated', onSelectionUpdated);
        }

        return () => {
            fabricObject.off('moving', onObjectMoving);
            fabricObject.off('scaling', onObjectScaling);
            fabricObject.off('rotating', onObjectRotating);

            if (fabricObject.type === 'activeSelection') {
                document.removeEventListener(
                    'scene:actor:selected',
                    onSelectionUpdated
                );
                document.removeEventListener(
                    'scene:actor:deselected',
                    onSelectionUpdated
                );
                fabricObject.canvas.off(
                    'selection:updated',
                    onSelectionUpdated
                );
            }
        };
    }, [fabricObject, updatePosition]);

    function handlePositionChange(x, y) {
        fabricObject
            .set({
                left: x,
                top: y
            })
            .setCoords();
        SceneManager.requestRenderAll();

        setPositionX(x);
        setPositionY(y);
    }

    function handleScaleChange(x, y) {
        if (!SceneManager.canvas.uniformScaling) {
            const aspectRatio = scaleX / scaleY;
            if (x !== scaleX) y = x / aspectRatio;
            else if (y !== scaleY) x = y * aspectRatio;
        }

        if (SceneManager.canvas.centeredScaling)
            fabricObject.centeredScaleTo(x, y);
        else
            fabricObject.set({
                scaleX: x,
                scaleY: y
            });

        _updatePosition(fabricObject);
        SceneManager.requestRenderAll();

        setScaleX(x);
        setScaleY(y);
    }

    function handleRotationChange(rotation) {
        fabricObject.rotate(rotation).setCoords();

        _updatePosition(fabricObject);
        SceneManager.requestRenderAll();

        setRotation(rotation);
    }

    function throttle(callback) {
        return _function.throttle(callback, 50);
    }
    function _updatePosition(fabricObject) {
        const newPosX = fabricObject.left;
        const newPosY = fabricObject.top;
        if (newPosX !== positionX) setPositionX(newPosX);
        if (newPosY !== positionY) setPositionY(newPosY);
    }

    return (
        <TransformPanelGroup
            positionX={positionX}
            positionY={positionY}
            scaleX={scaleX}
            scaleY={scaleY}
            rotation={rotation}
            onPositionChange={throttle(handlePositionChange)}
            onScaleChange={throttle(handleScaleChange)}
            onRotationChange={throttle(handleRotationChange)}
        />
    );
}

FabricTransformPanelGroup.propTypes = {
    fabricObject: PropTypes.object.isRequired
};

export default FabricTransformPanelGroup;
