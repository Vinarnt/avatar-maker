import React from 'react';
import PropTypes from 'prop-types';

import EditorPanelGroup from '../EditorPanelGroup';
import EditorPanelField2DVector from '@components/Editor/EditorPanelGroup/EditorPanelField/EditorPanelField2DVector/EditorPanelField2DVector';
import EditorPanelFieldNumberInput from '@components/Editor/EditorPanelGroup/EditorPanelField/EditorPanelFieldNumberInput/EditorPanelFieldNumberInput';

function TransformPanelGroup(props) {
    const {
        positionX,
        positionY,
        scaleX,
        scaleY,
        rotation,
        onPositionChange,
        onScaleChange,
        onRotationChange
    } = props;

    return (
        <EditorPanelGroup title="Transform">
            <EditorPanelField2DVector
                label="Position"
                x={positionX}
                y={positionY}
                step={1}
                precision={5}
                slideModifier={10000}
                onChange={onPositionChange}
            />
            <EditorPanelField2DVector
                label="Scale"
                x={scaleX}
                y={scaleY}
                step={0.0001}
                precision={5}
                slideModifier={100}
                onChange={onScaleChange}
            />
            <EditorPanelFieldNumberInput
                label="Rotation"
                value={rotation}
                min={0}
                max={360}
                step={1}
                precision={3}
                slideModifier={500}
                onChange={onRotationChange}
            />
        </EditorPanelGroup>
    );
}
TransformPanelGroup.propTypes = {
    positionX: PropTypes.number,
    positionY: PropTypes.number,
    scaleX: PropTypes.number,
    scaleY: PropTypes.number,
    rotation: PropTypes.number,
    onPositionChange: PropTypes.func.isRequired,
    onScaleChange: PropTypes.func.isRequired,
    onRotationChange: PropTypes.func.isRequired
};
TransformPanelGroup.defaultProps = {
    positionX: 0,
    positionY: 0,
    scaleX: 0,
    scaleY: 0,
    rotation: 0
};

export default TransformPanelGroup;
