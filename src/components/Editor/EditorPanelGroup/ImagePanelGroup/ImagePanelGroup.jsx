import React from 'react';
import { useStore } from 'react-redux';
import PropTypes from 'prop-types';

import EditorPanelGroup from '../EditorPanelGroup';
import EditorPanelFieldImageSource from '@components/Editor/EditorPanelGroup/EditorPanelField/EditorPanelFieldImageSource/EditorPanelFieldImageSource';
import SceneManager from '@services/SceneManager';

function ImagePanelGroup(props) {
    const { fabricObject } = props;
    const { dispatch } = useStore();

    function handleImageChange(name, url) {
        const imgElement = fabricObject.getElement();
        imgElement.addEventListener(
            'load',
            () => {
                const oldScaledWidth = fabricObject.getScaledWidth();
                const oldScaledHeight = fabricObject.getScaledHeight();

                fabricObject
                    .set({ width: imgElement.width, height: imgElement.height })
                    .scaleToCover(oldScaledWidth, oldScaledHeight)
                    .setCoords();
                SceneManager.requestRenderAll();
                fabricObject.trigger('scaling', {
                    target: fabricObject
                });
            },
            { once: true }
        );
        imgElement.src = url;
        fabricObject.actor.name = name;
        dispatch.scene.updateActor(fabricObject.actor);
    }

    return (
        <EditorPanelGroup title="Image">
            <EditorPanelFieldImageSource
                label="Image"
                imageName={fabricObject.actor.name}
                onChange={handleImageChange}
            />
        </EditorPanelGroup>
    );
}

ImagePanelGroup.propTypes = {
    fabricObject: PropTypes.object.isRequired
};

export default ImagePanelGroup;
