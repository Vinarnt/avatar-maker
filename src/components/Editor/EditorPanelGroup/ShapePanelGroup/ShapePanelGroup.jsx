import React, { useState } from 'react';
import PropTypes from 'prop-types';
import { fabric } from 'fabric';

import EditorPanelGroup from '../EditorPanelGroup';
import SceneManager from '@services/SceneManager';
import EditorPanelFieldFillColorInput from '@components/Editor/EditorPanelGroup/EditorPanelField/EditorPanelFieldFillColorInput/EditorPanelFieldFillColorInput';
import EditorPanelField2DVector from '@components/Editor/EditorPanelGroup/EditorPanelField/EditorPanelField2DVector/EditorPanelField2DVector';

function ShapePanelGroup(props) {
    const { fabricObject } = props;

    const [fill, setFill] = useState(
        new fabric.Color(fabricObject.fill || 'transparent').toRgbaObject()
    );
    const [rx, setRx] = useState(fabricObject.rx);
    const [ry, setRy] = useState(fabricObject.ry);

    function handleFillChange({ rgb }) {
        fabricObject.set('fill', fabric.Color.fromRgbaObject(rgb).toRgba());
        SceneManager.requestRenderAll();
        setFill(rgb);
    }

    function handleBorderRadiusChange(x, y) {
        fabricObject.set({ rx: x, ry: y });
        SceneManager.requestRenderAll();
        setRx(rx);
        setRy(ry);
    }

    return (
        <EditorPanelGroup title="Shape">
            <EditorPanelFieldFillColorInput
                label="Fill"
                value={fill}
                onChange={handleFillChange}
            />
            {fabricObject.type === 'rect' && (
                <EditorPanelField2DVector
                    label="Border radius"
                    x={rx}
                    y={ry}
                    precision={1}
                    step={0.1}
                    onChange={handleBorderRadiusChange}
                />
            )}
        </EditorPanelGroup>
    );
}

ShapePanelGroup.propTypes = {
    fabricObject: PropTypes.object.isRequired
};

export default ShapePanelGroup;
