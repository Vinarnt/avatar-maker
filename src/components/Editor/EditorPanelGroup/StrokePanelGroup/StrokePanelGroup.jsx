import React, { useState } from 'react';
import PropTypes from 'prop-types';
import { fabric } from 'fabric';

import EditorPanelGroup from '../EditorPanelGroup';
import SceneManager from '@services/SceneManager';
import EditorPanelFieldFillColorInput from '@components/Editor/EditorPanelGroup/EditorPanelField/EditorPanelFieldFillColorInput/EditorPanelFieldFillColorInput';
import EditorPanelFieldDropDown from '@components/Editor/EditorPanelGroup/EditorPanelField/EditorPanelFieldDropDown/EditorPanelFieldDropDown';
import EditorPanelFieldNumberInput from '@components/Editor/EditorPanelGroup/EditorPanelField/EditorPanelFieldNumberInput/EditorPanelFieldNumberInput';
import EditorPanelFieldCheckBox from '@components/Editor/EditorPanelGroup/EditorPanelField/EditorPanelFieldCheckBox/EditorPanelFieldCheckBox';

function StrokePanelGroup(props) {
    const { fabricObject } = props;

    const [stroke, setStroke] = useState(
        new fabric.Color(fabricObject.stroke || 'transparent').toRgbaObject()
    );
    const [strokeLineCap, setStrokeLineCap] = useState(
        fabricObject.strokeLineCap
    );
    const [strokeLineJoin, setStrokeLineJoin] = useState(
        fabricObject.strokeLineJoin
    );
    const [strokeMiterLimit, setStrokeMiterLimit] = useState(
        fabricObject.strokeMiterLimit
    );
    const [strokeUniform, setStrokeUniform] = useState(
        fabricObject.strokeUniform
    );
    const [strokeWidth, setStrokeWidth] = useState(fabricObject.strokeWidth);

    function handleStrokeChange({ rgb }) {
        fabricObject.set({ stroke: fabric.Color.fromRgbaObject(rgb).toRgba() });
        SceneManager.requestRenderAll();
        setStroke(rgb);
    }

    function handleStrokeLineCapChange(value) {
        fabricObject.set({ strokeLineCap: value });
        SceneManager.requestRenderAll();
        setStrokeLineCap(value);
    }

    function handleStrokeLineJoinChange(value) {
        fabricObject.set({ strokeLineJoin: value });
        SceneManager.requestRenderAll();
        setStrokeLineJoin(value);
    }

    function handleStrokeMiterLimitChange(value) {
        fabricObject.set({ strokeMiterLimit: value });
        SceneManager.requestRenderAll();
        setStrokeMiterLimit(value);
    }

    function handleStrokeWidthChange(value) {
        fabricObject.set({ strokeWidth: value });
        SceneManager.requestRenderAll();
        setStrokeWidth(value);
    }

    function handleStrokeUniformChange(state) {
        fabricObject.set({ strokeUniform: state });
        SceneManager.requestRenderAll();
        setStrokeUniform(state);
    }

    return (
        <EditorPanelGroup title="Stroke">
            <EditorPanelFieldFillColorInput
                label="Color"
                value={stroke}
                onChange={handleStrokeChange}
            />
            <EditorPanelFieldNumberInput
                label="Width"
                value={strokeWidth}
                onChange={handleStrokeWidthChange}
            />
            <EditorPanelFieldDropDown
                label="Line cap"
                value={strokeLineCap}
                options={{ butt: 'butt', round: 'round', square: 'square' }}
                onChange={handleStrokeLineCapChange}
            />
            <EditorPanelFieldDropDown
                label="Line join"
                value={strokeLineJoin}
                options={{ bevil: 'bevil', round: 'round', miter: 'miter' }}
                onChange={handleStrokeLineJoinChange}
            />
            {strokeLineJoin === 'miter' && (
                <EditorPanelFieldNumberInput
                    label="Miter limit"
                    vlaue={strokeMiterLimit}
                    onChange={handleStrokeMiterLimitChange}
                />
            )}
            <EditorPanelFieldCheckBox
                label="Uniform"
                value={strokeUniform}
                onChange={handleStrokeUniformChange}
            />
        </EditorPanelGroup>
    );
}

StrokePanelGroup.propTypes = {
    fabricObject: PropTypes.object.isRequired
};

export default StrokePanelGroup;
