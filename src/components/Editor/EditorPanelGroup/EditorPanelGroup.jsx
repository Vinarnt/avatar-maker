import React from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components';

import CollapsiblePanel from '@components/CollapsiblePanel/CollapsiblePanel';
import { getCSSColorDeclaration } from '@utils/color';

function EditorPanelGroup(props) {
    return (
        <Wrapper identifier={props.title} header={props.title}>
            {props.children}
        </Wrapper>
    );
}

EditorPanelGroup.propTypes = {
    title: PropTypes.string.isRequired
};

const Wrapper = styled(CollapsiblePanel)`
    .collapsible-panel-header {
        padding: 5px 10px;
        border-bottom: ${({ theme }) => theme.colors.background.primaryLight3}
            solid 1px;
        ${({ theme }) =>
            getCSSColorDeclaration(theme, theme.colors.background.primary)}

        &:hover {
            filter: brightness(80%);
        }
    }
`;

export default EditorPanelGroup;
