import React, { useEffect, useState } from 'react';
import PropTypes from 'prop-types';

import EditorPanelFieldCheckBox from '../EditorPanelField/EditorPanelFieldCheckBox/EditorPanelFieldCheckBox';
import EditorPanelGroup from '../EditorPanelGroup';
import EditorPanelFieldNumberInput from '@components/Editor/EditorPanelGroup/EditorPanelField/EditorPanelFieldNumberInput/EditorPanelFieldNumberInput';
import SceneManager from '@services/SceneManager';

function DisplayPanelGroup(props) {
    const { fabricObject } = props;

    const [opacity, setOpacity] = useState(fabricObject.opacity);
    const [depth, setDepth] = useState(fabricObject.getZIndex());
    const [visibility, setVisibility] = useState(fabricObject.visible);

    useEffect(() => {
        setOpacity(fabricObject.opacity);
        setDepth(fabricObject.getZIndex());
        setVisibility(fabricObject.visible);
    }, [fabricObject]);

    function handleOpacityChange(value) {
        fabricObject.set({ opacity: value });
        SceneManager.requestRenderAll();
        setOpacity(value);
    }

    function handleVisibilityChange(value) {
        fabricObject.visible = value;
        fabricObject.canvas.requestRenderAll();
        setVisibility(value);
    }

    function handleDepthChange(value) {
        fabricObject.moveTo(value);
        setDepth(value);
    }

    return (
        <EditorPanelGroup title="Display">
            <EditorPanelFieldCheckBox
                label="Visible"
                onChange={handleVisibilityChange}
                value={visibility}
            />
            <EditorPanelFieldNumberInput
                label="Opacity"
                value={opacity}
                min={0}
                max={1}
                precision={2}
                step={0.05}
                stepModifier={2}
                onChange={handleOpacityChange}
            />
            <EditorPanelFieldNumberInput
                label="Depth"
                value={depth}
                slideModifier={0.1}
                onChange={handleDepthChange}
            />
        </EditorPanelGroup>
    );
}

DisplayPanelGroup.propTypes = {
    fabricObject: PropTypes.object.isRequired
};

export default DisplayPanelGroup;
