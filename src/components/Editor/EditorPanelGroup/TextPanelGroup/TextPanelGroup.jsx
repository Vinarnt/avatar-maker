import React, { useState } from 'react';
import PropTypes from 'prop-types';
import { fabric } from 'fabric';

import EditorPanelGroup from '../EditorPanelGroup';
import EditorPanelFieldTextDecoration from '@components/Editor/EditorPanelGroup/EditorPanelField/EditorPanelFieldTextDecoration/EditorPanelFieldTextDecoration';
import SceneManager from '@services/SceneManager';
import EditorPanelFieldNumberInput from '@components/Editor/EditorPanelGroup/EditorPanelField/EditorPanelFieldNumberInput/EditorPanelFieldNumberInput';
import EditorPanelFieldFillColorInput from '@components/Editor/EditorPanelGroup/EditorPanelField/EditorPanelFieldFillColorInput/EditorPanelFieldFillColorInput';
import EditorPanelFieldDropDown from '@components/Editor/EditorPanelGroup/EditorPanelField/EditorPanelFieldDropDown/EditorPanelFieldDropDown';
import { availableFonts } from '@utils/fonts';

function TextPanelGroup(props) {
    const { fabricObject } = props;

    const [isUnderlined, setUnderlined] = useState(fabricObject.underline);
    const [isOverlined, setOverlined] = useState(fabricObject.overline);
    const [isLinethrough, setLinethrough] = useState(fabricObject.linethrough);
    const [fontFamily, setFontFamily] = useState(fabricObject.fontFamily);
    const [fontSize, setFontSize] = useState(fabricObject.fontSize);
    const [color, setColor] = useState(
        new fabric.Color(fabricObject.fill || 'transparent').toRgbaObject()
    );
    const [textAlign, setTextAlign] = useState(fabricObject.textAlign);

    function handleUnderlineChange(state) {
        fabricObject.set({ underline: state });
        SceneManager.requestRenderAll();
        setUnderlined(state);
    }
    function handleOverlineChange(state) {
        fabricObject.set({ overline: state });
        SceneManager.requestRenderAll();
        setOverlined(state);
    }
    function handleLinethroughChange(state) {
        fabricObject.set({ linethrough: state });
        SceneManager.requestRenderAll();
        setLinethrough(state);
    }

    function handleFontFamilyChange(value) {
        fabricObject.set({ fontFamily: value });
        SceneManager.requestRenderAll();
        setFontFamily(value);
    }

    function handleTextAlignChange(value) {
        fabricObject.set({ textAlign: value });
        SceneManager.requestRenderAll();
        setTextAlign(value);
    }

    function handleFontSizeChange(value) {
        fabricObject.set({ fontSize: value });
        SceneManager.requestRenderAll();
        setFontSize(value);
    }

    function handleColorChange({ rgb }) {
        fabricObject.set({ fill: fabric.Color.fromRgbaObject(rgb).toRgba() });
        SceneManager.requestRenderAll();
        setColor(rgb);
    }

    return (
        <EditorPanelGroup title="Text">
            <EditorPanelFieldTextDecoration
                label="Decoration"
                isUnderlined={isUnderlined}
                isOverlined={isOverlined}
                isLinethrough={isLinethrough}
                onUnderlineChange={handleUnderlineChange}
                onOverlineChange={handleOverlineChange}
                onLinethroughChange={handleLinethroughChange}
            />
            <EditorPanelFieldDropDown
                label="Font family"
                controlStyle={{ fontFamily: fontFamily }}
                value={fontFamily}
                onChange={handleFontFamilyChange}
            >
                {availableFonts.map(font => (
                    <option
                        key={font}
                        value={font}
                        style={{ fontFamily: font }}
                    >
                        {font}
                    </option>
                ))}
            </EditorPanelFieldDropDown>
            <EditorPanelFieldDropDown
                label="Text align"
                value={textAlign}
                options={{
                    left: 'Left',
                    center: 'Center',
                    right: 'Right',
                    justify: 'Justify',
                    'justify-left': 'Justify left',
                    'justify-center': 'Justify center',
                    'justify-right': 'Justify right'
                }}
                onChange={handleTextAlignChange}
            />
            <EditorPanelFieldNumberInput
                label="Font size"
                value={fontSize}
                min={5}
                max={200}
                onChange={handleFontSizeChange}
            />
            <EditorPanelFieldFillColorInput
                label="Color"
                value={color}
                onChange={handleColorChange}
            />
        </EditorPanelGroup>
    );
}

TextPanelGroup.propTypes = {
    fabricObject: PropTypes.object.isRequired
};

export default TextPanelGroup;
