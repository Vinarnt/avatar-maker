import React from 'react';
import PropTypes from 'prop-types';

import EditorPanelField from '../EditorPanelField';

function EditorPanelFieldTextInput(props) {
    const {
        value,
        onChange,
        minLength,
        maxLength,
        pattern,
        placeholder,
        readOnly,
        size,
        spellCheck
    } = props;

    function handleChange(event) {
        onChange(event, event.target.value);
    }

    return (
        <EditorPanelField label={props.label}>
            <input
                type="text"
                value={value}
                onChange={handleChange}
                minLength={minLength}
                maxLength={maxLength}
                pattern={pattern}
                placeholder={placeholder}
                readOnly={readOnly}
                size={size}
                spellCheck={spellCheck}
            />
        </EditorPanelField>
    );
}

EditorPanelFieldTextInput.propTypes = {
    label: PropTypes.string.isRequired,
    value: PropTypes.string,
    onChange: PropTypes.func.isRequired,
    minLength: PropTypes.number,
    maxLength: PropTypes.number,
    pattern: PropTypes.string,
    placeholder: PropTypes.string,
    readOnly: PropTypes.bool,
    size: PropTypes.number,
    spellCheck: PropTypes.bool
};

export default EditorPanelFieldTextInput;
