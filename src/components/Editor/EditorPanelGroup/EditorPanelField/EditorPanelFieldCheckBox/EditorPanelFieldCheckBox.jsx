import React from 'react';
import PropTypes from 'prop-types';

import EditorPanelField from '../EditorPanelField';

function EditorPanelFieldCheckBox(props) {
    const { label, value, disabled, onChange } = props;

    const handleChange = e => onChange(e.target.checked);

    return (
        <EditorPanelField label={label}>
            <input
                type="checkbox"
                checked={value}
                disabled={disabled}
                onChange={handleChange}
            />
        </EditorPanelField>
    );
}

EditorPanelFieldCheckBox.propTypes = {
    label: PropTypes.string.isRequired,
    value: PropTypes.bool,
    disabled: PropTypes.bool,
    onChange: PropTypes.func.isRequired
};
EditorPanelFieldCheckBox.defaultProps = {
    value: false,
    disabled: false
};

export default EditorPanelFieldCheckBox;
