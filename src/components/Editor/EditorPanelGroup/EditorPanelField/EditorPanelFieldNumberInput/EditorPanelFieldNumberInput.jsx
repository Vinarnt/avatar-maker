import React from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components';
import InputNumberEditor from 'react-input-number-editor';

import EditorPanelField from '../EditorPanelField';

function EditorPanelFieldNumberInput(props) {
    const {
        value,
        min,
        max,
        step,
        stepModifier,
        precision,
        slideModifier,
        onChange
    } = props;

    return (
        <EditorPanelField label={props.label}>
            <InputWrapper
                value={value}
                min={min}
                max={max}
                step={step}
                stepModifier={stepModifier}
                precision={precision}
                slideModifier={slideModifier}
                onChange={onChange}
            />
        </EditorPanelField>
    );
}

EditorPanelFieldNumberInput.propTypes = {
    label: PropTypes.string.isRequired,
    value: PropTypes.number,
    min: PropTypes.number,
    max: PropTypes.number,
    step: PropTypes.number,
    stepModifier: PropTypes.number,
    precision: PropTypes.number,
    slideModifier: PropTypes.number,
    onChange: PropTypes.func.isRequired
};

const InputWrapper = styled(InputNumberEditor)`
    width: 100%;
`;

export default EditorPanelFieldNumberInput;
