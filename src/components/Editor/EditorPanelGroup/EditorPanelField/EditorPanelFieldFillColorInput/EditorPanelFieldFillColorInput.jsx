import React from 'react';
import PropTypes from 'prop-types';

import EditorPanelField from '../EditorPanelField';
import ColorInput from '@components/Input/ColorInput';

function EditorPanelFieldFillColorInput(props) {
    const { value, onChange } = props;

    return (
        <EditorPanelField label={props.label}>
            <ColorInput value={value} onChange={onChange} />
        </EditorPanelField>
    );
}

EditorPanelFieldFillColorInput.propTypes = {
    label: PropTypes.string.isRequired,
    value: PropTypes.oneOfType([PropTypes.object, PropTypes.string]).isRequired,
    onChange: PropTypes.func.isRequired
};

export default EditorPanelFieldFillColorInput;
