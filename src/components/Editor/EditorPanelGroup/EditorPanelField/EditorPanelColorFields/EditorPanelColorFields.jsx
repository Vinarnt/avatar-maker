import React, { useState } from 'react';
import PropTypes from 'prop-types';
import { desaturate, lighten } from 'polished';
import { fabric } from 'fabric';

import EditorPanelFieldFillColorInput from '@components/Editor/EditorPanelGroup/EditorPanelField/EditorPanelFieldFillColorInput/EditorPanelFieldFillColorInput';
import EditorPanelFieldColorTypeSelect from '@components/Editor/EditorPanelGroup/EditorPanelField/EditorPanelFieldColorTypeSelect/EditorPanelFieldColorTypeSelect';
import EditorPanelFieldGradientColor from '@components/Editor/EditorPanelGroup/EditorPanelField/EditorPanelFieldGradientColor/EditorPanelFieldGradientColor';
import SceneManager from '@services/SceneManager';
import EditorPanelField2DVector from '@components/Editor/EditorPanelGroup/EditorPanelField/EditorPanelField2DVector/EditorPanelField2DVector';

function EditorPanelColorFields({ actor }) {
    const { fabricObject } = actor;
    const [color, setColor] = useState(getColor(fabricObject.fill));
    const [colorType, setColorType] = useState(
        fabricObject.fill instanceof fabric.Gradient ? 'gradient' : 'fill'
    );
    const [gradientStart, setGradientStart] = useState(
        getGradientStart(fabricObject.fill)
    );
    const [gradientEnd, setGradientEnd] = useState(
        getGradientEnd(fabricObject.fill)
    );

    function getColor(fillColor) {
        if (fillColor instanceof fabric.Gradient) {
            const colorStops = {};
            fillColor.colorStops.forEach(({ offset, color, opacity }) => {
                const _color = new fabric.Color(color);
                _color.setAlpha(opacity);

                colorStops[`${offset}`] = _color.toRgba();
            });
            return colorStops;
        } else {
            return new fabric.Color(fillColor).toRgba();
        }
    }

    function handleColorTypeChange(type) {
        let _color;
        if (type === 'fill') {
            _color = color[0];
            fabricObject.set('fill', _color);
        } else {
            _color = {
                0: color,
                1: lighten(0.2, desaturate(0.8, color))
            };
            fabricObject.setGradient('fill', {
                x1: 0,
                y1: 0,
                x2: fabricObject.width,
                y2: fabricObject.height,
                colorStops: _color
            });
        }
        setColorType(type);
        setColor(_color);
        setGradientStart([0, 0]);
        setGradientEnd([fabricObject.width, fabricObject.height]);
        SceneManager.requestRenderAll();
    }

    function handleFillColorChange(newColor) {
        const rgbaColor = fabric.Color.fromRgbaObject(newColor.rgb).toRgba();
        fabricObject.set('fill', rgbaColor);
        setColor(rgbaColor);
        SceneManager.requestRenderAll();
    }

    function handleGradientColorChange(colorStops) {
        fabricObject.setGradient('fill', {
            ...fabricObject.fill.coords,
            colorStops
        });
        setColor(colorStops);
        SceneManager.requestRenderAll();
    }

    function handleGradientStartChange(x, y) {
        fabricObject.setGradient('fill', {
            x1: x,
            y1: y,
            x2: gradientEnd[0],
            y2: gradientEnd[1],
            colorStops: getColor(fabricObject.fill)
        });
        setGradientStart([x, y]);
        SceneManager.requestRenderAll();
    }

    function handleGradientEndChange(x, y) {
        fabricObject.setGradient('fill', {
            x1: gradientStart[0],
            y1: gradientStart[1],
            x2: x,
            y2: y,
            colorStops: getColor(fabricObject.fill)
        });
        setGradientEnd([x, y]);
        SceneManager.requestRenderAll();
    }

    function getGradientStart(fill) {
        if (colorType === 'gradient') {
            return [fill.x1, fill.y1];
        }
    }

    function getGradientEnd(fill) {
        if (colorType === 'gradient') {
            return [fill.x2, fill.y2];
        }
    }

    function getColorComponent() {
        switch (colorType) {
            case 'fill':
                return (
                    <EditorPanelFieldFillColorInput
                        label="Color"
                        value={color}
                        onChange={handleFillColorChange}
                    />
                );
            case 'gradient':
                return (
                    <>
                        <EditorPanelFieldGradientColor
                            label="Colors"
                            colorStops={color}
                            onChange={handleGradientColorChange}
                        />
                        <EditorPanelField2DVector
                            label="Start"
                            x={gradientStart[0]}
                            y={gradientStart[1]}
                            onChange={handleGradientStartChange}
                        />
                        <EditorPanelField2DVector
                            label="End"
                            x={gradientEnd[0]}
                            y={gradientEnd[1]}
                            onChange={handleGradientEndChange}
                        />
                    </>
                );
            default:
                throw new Error('Color type should be fill or gradient.');
        }
    }

    return (
        <>
            <EditorPanelFieldColorTypeSelect
                label={'Color type'}
                onChange={handleColorTypeChange}
                value={colorType}
            />
            {getColorComponent()}
        </>
    );
}

EditorPanelColorFields.propTypes = {
    actor: PropTypes.object.isRequired
};

export default EditorPanelColorFields;
