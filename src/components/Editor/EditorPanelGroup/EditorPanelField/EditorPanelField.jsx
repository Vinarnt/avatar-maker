import React from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components';

import { getCSSColorDeclaration } from '@utils/color';

function EditorPanelField({ className, label, children }) {
    return (
        <Wrapper className={className}>
            <Label className="label">{label}</Label>
            <Control className="control">{children}</Control>
        </Wrapper>
    );
}

EditorPanelField.propTypes = {
    className: PropTypes.string,
    label: PropTypes.string.isRequired
};

const Wrapper = styled.div`
    display: grid;
    grid-template-columns: 100px 1fr;
    ${({ theme }) =>
        getCSSColorDeclaration(theme, theme.colors.background.primaryLight2)};
    color: white;
`;

const Label = styled.div`
    padding: 5px 10px 5px 10px;
`;

const Control = styled(Label)`
    border-left: 1px solid grey;
`;

export default EditorPanelField;
