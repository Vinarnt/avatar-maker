import React from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components';
import { fabric } from 'fabric';

import EditorPanelField from '@components/Editor/EditorPanelGroup/EditorPanelField/EditorPanelField';
import ColorInput from '@components/Input/ColorInput';

function EditorPanelFieldGradientColor({ label, colorStops, onChange }) {
    function handleColorChange(offset, color) {
        const newColorStops = { ...colorStops };
        newColorStops[`${offset}`] = fabric.Color.fromRgbaObject(
            color
        ).toRgba();
        onChange(newColorStops);
    }

    return (
        <EditorPanelField label={label}>
            <List>
                {Object.entries(colorStops).map(([offset, color], index) => (
                    <Item key={`item-${index}`}>
                        <ColorInput
                            value={new fabric.Color(color).toRgbaObject()}
                            onChange={color =>
                                handleColorChange(offset, color.rgb)
                            }
                        />
                    </Item>
                ))}
            </List>
        </EditorPanelField>
    );
}

EditorPanelFieldGradientColor.propTypes = {
    label: PropTypes.string.isRequired,
    colorStops: PropTypes.object.isRequired,
    onChange: PropTypes.func.isRequired
};
const List = styled.ul`
    margin: 0;
    padding: 0;
    list-style: none;
`;

const Item = styled.li``;

export default EditorPanelFieldGradientColor;
