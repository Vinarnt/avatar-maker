import React from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components';
import InputNumberEditor from 'react-input-number-editor';

import EditorPanelField from '../EditorPanelField';
import { getCSSColorDeclaration } from '@utils/color';

function EditorPanelField2DVector(props) {
    const { x, y, step, precision, slideModifier, onChange } = props;

    const handleChangeX = value => onChange(value, y);

    const handleChangeY = value => onChange(x, value);

    return (
        <Wrapper label={props.label}>
            <InputGroup>
                <span>x</span>
                <InputNumberEditor
                    value={x}
                    step={step}
                    precision={precision}
                    slideModifier={slideModifier}
                    onChange={handleChangeX}
                />
            </InputGroup>
            <InputGroup>
                <span>y</span>
                <InputNumberEditor
                    value={y}
                    step={step}
                    precision={precision}
                    slideModifier={slideModifier}
                    onChange={handleChangeY}
                />
            </InputGroup>
        </Wrapper>
    );
}

EditorPanelField2DVector.propTypes = {
    label: PropTypes.string.isRequired,
    x: PropTypes.number,
    y: PropTypes.number,
    step: PropTypes.number,
    precision: PropTypes.number,
    slideModifier: PropTypes.number,
    onChange: PropTypes.func.isRequired
};
EditorPanelField2DVector.defaultProps = {
    x: 0,
    y: 0,
    step: 1,
    precision: 0
};

const Wrapper = styled(EditorPanelField)`
    & > :nth-child(2) {
        display: grid;
        grid-template-columns: 1fr 1fr;
        grid-template-rows: min-content;
        grid-column-gap: ${({ theme }) => theme.spacings.xs};
    }
`;

const InputGroup = styled.div`
    display: flex;
    ${props =>
        getCSSColorDeclaration(
            props.theme,
            props.theme.colors.background.primary
        )};
    border: 1px solid ${({ theme }) => theme.colors.background.primaryLight5};

    & > :first-child {
        border-right: 1px solid
            ${({ theme }) => theme.colors.background.primaryLight5};
        text-transform: uppercase;
        font-weight: bold;
        font-size: ${({ theme }) => theme.sizes.xs};
        line-height: 20px;
        padding: 0 ${({ theme }) => theme.spacings.xs};
    }

    & > :nth-child(2) {
        border: none;
        width: 100%;
        overflow: hidden;
    }
`;

export default EditorPanelField2DVector;
