import React from 'react';
import PropTypes from 'prop-types';

import EditorPanelFieldDropDown from '@components/Editor/EditorPanelGroup/EditorPanelField/EditorPanelFieldDropDown/EditorPanelFieldDropDown';

function EditorPanelFieldColorTypeSelect({ label, value, onChange }) {
    return (
        <EditorPanelFieldDropDown
            label="Color type"
            value={value}
            options={{ fill: 'Fill', gradient: 'Gradient' }}
            onChange={onChange}
        />
    );
}

EditorPanelFieldColorTypeSelect.propTypes = {
    label: PropTypes.string.isRequired,
    value: PropTypes.string.isRequired,
    onChange: PropTypes.func.isRequired
};

export default EditorPanelFieldColorTypeSelect;
