import React from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components';

import EditorPanelField from '../EditorPanelField';
import ToggleButton from '@components/Button/ToggleButton';

function EditorPanelFieldTextDecoration(props) {
    const {
        isUnderlined,
        isOverlined,
        isLinethrough,
        onUnderlineChange,
        onOverlineChange,
        onLinethroughChange
    } = props;

    return (
        <EditorPanelField label={props.label}>
            <ToggleButton active={isUnderlined} onChange={onUnderlineChange}>
                <DecorationText className="underline">Text</DecorationText>
            </ToggleButton>
            <ToggleButton active={isOverlined} onChange={onOverlineChange}>
                <DecorationText className="overline">Text</DecorationText>
            </ToggleButton>
            <ToggleButton active={isLinethrough} onChange={onLinethroughChange}>
                <DecorationText className="line-through">Text</DecorationText>
            </ToggleButton>
        </EditorPanelField>
    );
}

EditorPanelFieldTextDecoration.propTypes = {
    label: PropTypes.string.isRequired,
    isUnderlined: PropTypes.bool,
    isOverlined: PropTypes.bool,
    isLinethrough: PropTypes.bool,
    onUnderlineChange: PropTypes.func.isRequired,
    onOverlineChange: PropTypes.func.isRequired,
    onLinethroughChange: PropTypes.func.isRequired
};
EditorPanelFieldTextDecoration.defaultProps = {
    isUnderlined: false,
    isOverlined: false,
    isLinethrough: false
};

const DecorationText = styled.div`
    font-weight: bold;
`;

export default EditorPanelFieldTextDecoration;
