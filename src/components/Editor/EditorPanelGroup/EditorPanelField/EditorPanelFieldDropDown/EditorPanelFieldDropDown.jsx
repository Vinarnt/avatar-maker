import React from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components';

import EditorPanelField from '../EditorPanelField';

function EditorPanelFieldDropDown(props) {
    const {
        label,
        value,
        options,
        size,
        disabled,
        onChange,
        controlStyle,
        children
    } = props;

    function handleChange({ target: { value } }) {
        onChange(value);
    }

    return (
        <EditorPanelField
            style={controlStyle}
            label={label}
            size={size}
            disabled={disabled}
        >
            <SelectWrapper
                style={controlStyle}
                value={value}
                onChange={handleChange}
            >
                {!children &&
                    Object.entries(options).map(([_key, _value]) => (
                        <option key={_key} value={_key}>
                            {_value}
                        </option>
                    ))}
                {children}
            </SelectWrapper>
        </EditorPanelField>
    );
}

EditorPanelFieldDropDown.propTypes = {
    label: PropTypes.string.isRequired,
    value: PropTypes.string,
    options: PropTypes.object,
    size: PropTypes.number,
    disabled: PropTypes.bool,
    controlStyle: PropTypes.object,
    onChange: PropTypes.func.isRequired
};

const SelectWrapper = styled.select`
    width: 100%;
`;

export default EditorPanelFieldDropDown;
