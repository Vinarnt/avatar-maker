import React, { useState } from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components';

import EditorPanelField from '../EditorPanelField';
import ImageChooserModal from '@components/Modal/ImageChooserModal';
import Button from '@components/Button/Button';

function EditorPanelFieldImageSource(props) {
    const { label, imageName, onChange } = props;

    const [isOpen, setOpen] = useState(false);

    function handleClick() {
        setOpen(!isOpen);
    }

    function handleRequestClose() {
        setOpen(false);
    }

    function handleConfirm(name, url) {
        setOpen(false);
        if (url) onChange(name, url);
    }

    return (
        <>
            <EditorPanelField label={label}>
                <ButtonWrapper onClick={handleClick}>{imageName}</ButtonWrapper>
            </EditorPanelField>
            <ImageChooserModal
                open={isOpen}
                onRequestClose={handleRequestClose}
                onCancel={handleRequestClose}
                onConfirm={handleConfirm}
            />
        </>
    );
}
EditorPanelFieldImageSource.propTypes = {
    label: PropTypes.string.isRequired,
    imageName: PropTypes.string.isRequired,
    onChange: PropTypes.func
};

const ButtonWrapper = styled(Button)`
    max-width: 14em;
    border-color: ${({ theme }) => theme.colors.background.primaryLight1};
    white-space: nowrap;
    overflow: hidden;
    text-overflow: ellipsis;

    &:hover {
        border-color: ${({ theme }) => theme.colors.background.primaryLight5};
    }
`;

export default EditorPanelFieldImageSource;
