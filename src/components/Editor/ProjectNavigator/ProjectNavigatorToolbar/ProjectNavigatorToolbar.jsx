import React from 'react';
import styled from 'styled-components';
import { IconContext } from 'react-icons';

import AddButton from '@components/Button/ProjectNavigatorToolbar/AddButton';
import RemoveButton from '@components/Button/ProjectNavigatorToolbar/RemoveButton';
import SettingButton from '@components/Button/ProjectNavigatorToolbar/SettingButton';
import SaveButton from '@components/Button/ProjectNavigatorToolbar/SaveButton';

function ProjectNavigatorToolbar() {
    return (
        <Wrapper>
            <IconContext.Provider value={{ size: '1em' }}>
                <AddButton />
                <RemoveButton />
                <SaveButton />
                <SettingButton />
            </IconContext.Provider>
        </Wrapper>
    );
}

const toolbarHeight = '35px';
const Wrapper = styled.div`
    height: ${toolbarHeight};
    background-color: ${({ theme }) => theme.colors.background.primaryLight2};

    & > * {
        display: inline-block;
    }

    & button {
        min-width: ${toolbarHeight};
        height: ${toolbarHeight};
        border: none;
        background-color: ${({ theme }) =>
            theme.colors.background.primaryLight2};

        &:hover {
            background-color: ${({ theme }) =>
                theme.colors.background.primaryLight3};
        }
    }
`;

export default ProjectNavigatorToolbar;
