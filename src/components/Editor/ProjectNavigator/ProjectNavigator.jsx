import React from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components';
import { MenuProvider } from 'react-contexify';

import ProjectNavigatorItemContextMenu from '@components/ContextMenu/ProjectNavigatorContextMenu/ProjectNavigatorItemContextMenu';
import ProjectNavigatorContextMenu from '@components/ContextMenu/ProjectNavigatorContextMenu/ProjectNavigatorContextMenu';
import ProjectNavigatorToolbar from '@components/Editor/ProjectNavigator/ProjectNavigatorToolbar/ProjectNavigatorToolbar';

function ProjectNavigator(props) {
    return (
        <NavigatorContainer>
            <ProjectNavigatorToolbar />
            <MenuProvider id={ProjectNavigatorContextMenu.TYPE}>
                <NavigatorContent>{props.children}</NavigatorContent>
            </MenuProvider>
            <ProjectNavigatorItemContextMenu />
            <ProjectNavigatorContextMenu />
        </NavigatorContainer>
    );
}
ProjectNavigator.propTypes = {
    selected: PropTypes.object
};

const NavigatorContainer = styled.div`
    border-top: ${({ theme }) => theme.colors.background.primary} 1px solid;
`;

const NavigatorContent = styled.div`
    height: 150px;
    overflow-y: auto;
    overflow-x: hidden;
    background-color: ${({ theme }) => theme.colors.background.primaryLight3};

    @media (min-width: ${({ theme }) => theme.breakpoints.sm}px) {
        height: 250px;
    }
`;

export default ProjectNavigator;
