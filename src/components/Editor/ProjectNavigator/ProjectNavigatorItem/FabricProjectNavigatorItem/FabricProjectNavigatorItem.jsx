import ProjectNavigatorItem from '../ProjectNavigatorItem';
import React from 'react';
import PropTypes from 'prop-types';

function FabricProjectNavigatorItem(props) {
    const { actor } = props;

    return (
        <ProjectNavigatorItem
            {...props}
            title={actor.name}
        />
    );
}
FabricProjectNavigatorItem.propTypes = {
    actor: PropTypes.object.isRequired
};

export default FabricProjectNavigatorItem;
