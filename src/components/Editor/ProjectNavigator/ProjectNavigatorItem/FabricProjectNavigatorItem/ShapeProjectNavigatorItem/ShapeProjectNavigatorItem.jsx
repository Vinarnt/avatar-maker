import React from 'react';
import PropTypes from 'prop-types';

import ShapeIcon from '@assets/icons/shapes.svg';
import FabricProjectNavigatorItem from '../FabricProjectNavigatorItem';

function ShapeProjectNavigatorItem(props) {
    const { actor } = props;
    return (
        <FabricProjectNavigatorItem {...props} actor={actor} icon={ShapeIcon} />
    );
}
ShapeProjectNavigatorItem.propTypes = {
    actor: PropTypes.object.isRequired
};

export default ShapeProjectNavigatorItem;
