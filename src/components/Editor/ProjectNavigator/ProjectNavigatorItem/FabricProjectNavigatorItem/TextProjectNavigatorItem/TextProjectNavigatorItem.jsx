import React from 'react';
import PropTypes from 'prop-types';

import TextIcon from '@assets/icons/baseline-text_format-24px.svg';
import FabricProjectNavigatorItem from '../FabricProjectNavigatorItem';

function TextProjectNavigatorItem(props) {
    const { actor } = props;
    return (
        <FabricProjectNavigatorItem {...props} actor={actor} icon={TextIcon} />
    );
}
TextProjectNavigatorItem.propTypes = {
    actor: PropTypes.object.isRequired
};

export default TextProjectNavigatorItem;
