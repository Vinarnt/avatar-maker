import React, { useEffect, useState } from 'react';
import PropTypes from 'prop-types';

import FabricProjectNavigatorItem from '../FabricProjectNavigatorItem';
import RingSpinner from '@components/ProgressBar/Spinner/RingSpinner';

function ImageProjectNavigatorItem(props) {
    const { actor } = props;
    const [imageSrc, setImageSrc] = useState(actor.fabricObject?.getSrc(false));

    useEffect(() => {
        if (actor.fabricObject) setImageSrc(actor.fabricObject.getSrc(false));
    }, [actor.fabricObject]);

    return (
        <FabricProjectNavigatorItem
            {...props}
            actor={actor}
            icon={
                imageSrc ? (
                    <img alt="" src={imageSrc} />
                ) : (
                    <RingSpinner size="80%" />
                )
            }
        />
    );
}
ImageProjectNavigatorItem.propTypes = {
    actor: PropTypes.object.isRequired
};

export default ImageProjectNavigatorItem;
