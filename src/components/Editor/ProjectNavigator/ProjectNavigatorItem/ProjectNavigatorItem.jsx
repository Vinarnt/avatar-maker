import React from 'react';
import PropTypes from 'prop-types';
import { MenuProvider } from 'react-contexify';

import './ProjectNavigatorItem.scss';
import SceneManager from '@services/SceneManager';
import ProjectNavigatorItemContextMenu from '@components/ContextMenu/ProjectNavigatorContextMenu/ProjectNavigatorItemContextMenu';

function ProjectNavigatorItem(props) {
    const { icon, title, actor, selected, onClick } = props;

    const handleItemClick = event => {
        if (event.shiftKey) SceneManager.toggleActorSelection(actor);
        else SceneManager.selectActor(actor);

        if (onClick) onClick(event);
    };
    return (
        <MenuProvider
            id={ProjectNavigatorItemContextMenu.TYPE}
            render={({ children, ...rest }) => (
                <div
                    {...rest}
                    className={`navigator-item${selected ? ' active' : ''}`}
                    onClick={handleItemClick}
                >
                    {children}
                </div>
            )}
            data={{ actor: actor }}
        >
            <div className="navigator-item-icon">
                {typeof icon === 'string' ? (
                    <img src={icon} alt={title} />
                ) : (
                    icon
                )}
            </div>
            <div className="navigator-item-title">{title}</div>
        </MenuProvider>
    );
}

ProjectNavigatorItem.propTypes = {
    icon: PropTypes.oneOfType([PropTypes.element, PropTypes.string]),
    title: PropTypes.string,
    actor: PropTypes.object.isRequired,
    selected: PropTypes.bool,
    onClick: PropTypes.func
};

export default ProjectNavigatorItem;
