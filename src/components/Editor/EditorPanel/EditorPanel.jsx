import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import styled from 'styled-components';

import SceneManager from '@services/SceneManager';
import store from '@store/store';
import ProjectNavigator from '@components/Editor/ProjectNavigator/ProjectNavigator';
import ImageProjectNavigatorItem from '@components/Editor/ProjectNavigator/ProjectNavigatorItem/FabricProjectNavigatorItem/ImageProjectNavigatorItem/ImageProjectNavigatorItem';
import ShapeProjectNavigatorItem from '@components/Editor/ProjectNavigator/ProjectNavigatorItem/FabricProjectNavigatorItem/ShapeProjectNavigatorItem/ShapeProjectNavigatorItem';
import FabricProjectNavigatorItem from '@components/Editor/ProjectNavigator/ProjectNavigatorItem/FabricProjectNavigatorItem/FabricProjectNavigatorItem';
import FabricTransformPanelGroup from '@components/Editor/EditorPanelGroup/TransformPanelGroup/FabricTransformPanelGroup/FabricTransformPanelGroup';
import DisplayPanelGroup from '@components/Editor/EditorPanelGroup/DisplayPanelGroup/DisplayPanelGroup';
import TextProjectNavigatorItem from '@components/Editor/ProjectNavigator/ProjectNavigatorItem/FabricProjectNavigatorItem/TextProjectNavigatorItem/TextProjectNavigatorItem';
import TextPanelGroupComposition from '@components/Editor/EditorPanel/EditorPanelGroupComposition/TextPanelGroupComposition';
import ShapePanelGroupComposition from '@components/Editor/EditorPanel/EditorPanelGroupComposition/ShapePanelGroupComposition';
import ImagePanelGroupComposition from '@components/Editor/EditorPanel/EditorPanelGroupComposition/ImagePanelGroupComposition';
import BorderPanelGroupComposition from '@components/Editor/EditorPanel/EditorPanelGroupComposition/BorderPanelGroupComposition';

function EditorPanel(props) {
    const { actors, selectedActors, isSelected, selectionCount } = props;

    const getNavigatorItemList = actors => {
        return actors.map(actor => {
            const itemProps = {
                key: actor.id,
                actor: actor,
                selected: isSelected(actor)
            };

            switch (actor.type) {
                case 'image':
                    return <ImageProjectNavigatorItem {...itemProps} />;
                case 'text':
                    return <TextProjectNavigatorItem {...itemProps} />;
                case 'shape':
                    return <ShapeProjectNavigatorItem {...itemProps} />;
                default:
                    return <FabricProjectNavigatorItem {...itemProps} />;
            }
        });
    };

    const getPanelGroupList = (selectionCount, selectedActors) => {
        if (selectionCount === 1) {
            const selectedActor = selectedActors[0];
            if (selectedActor.fabricObject) {
                switch (selectedActor.type) {
                    case 'image':
                        return (
                            <ImagePanelGroupComposition
                                fabricObject={selectedActor.fabricObject}
                            />
                        );
                    case 'text':
                        return (
                            <TextPanelGroupComposition
                                fabricObject={selectedActor.fabricObject}
                            />
                        );
                    case 'shape':
                        return (
                            <ShapePanelGroupComposition
                                fabricObject={selectedActor.fabricObject}
                            />
                        );
                    case 'border':
                        return (
                            <BorderPanelGroupComposition
                                actor={selectedActor}
                            />
                        );
                    default:
                        return (
                            <>
                                <FabricTransformPanelGroup
                                    fabricObject={selectedActor.fabricObject}
                                />
                                <DisplayPanelGroup
                                    fabricObject={selectedActor.fabricObject}
                                />
                            </>
                        );
                }
            }
        } else if (selectionCount > 1) {
            return (
                <FabricTransformPanelGroup
                    fabricObject={SceneManager.canvas.getActiveObject()}
                />
            );
        }
    };

    return (
        <Wrapper {...props}>
            <ProjectNavigator>{getNavigatorItemList(actors)}</ProjectNavigator>

            <div id="panel-group-container">
                {getPanelGroupList(selectionCount(), selectedActors)}
            </div>
        </Wrapper>
    );
}
EditorPanel.propTypes = {
    className: PropTypes.string,
    actors: PropTypes.array,
    selectedActors: PropTypes.array,
    isSelected: PropTypes.func,
    selectionCount: PropTypes.func
};

const Wrapper = styled.div`
    display: flex;
    flex-direction: column;
    overflow: hidden;
    background-color: ${({ theme }) => theme.colors.background.primary};
    color: white;
    height: 100%;

    @media (max-width: ${({ theme }) => theme.breakpoints.sm}px) {
        opacity: 0.75;
    }

    #panel-group-container {
        overflow-y: auto;
        overflow-x: hidden;
    }
`;

const mapStateToProps = state => ({
    actors: state.scene.actors,
    selectedActors: state.scene.selectedActors,
    isSelected: actor => store.select.scene.isSelected(actor)(state),
    selectionCount: () => store.select.scene.selectionCount(state)
});

export default connect(mapStateToProps, null)(EditorPanel);
