import React from 'react';
import PropTypes from 'prop-types';

import FabricTransformPanelGroup from '@components/Editor/EditorPanelGroup/TransformPanelGroup/FabricTransformPanelGroup/FabricTransformPanelGroup';
import DisplayPanelGroup from '@components/Editor/EditorPanelGroup/DisplayPanelGroup/DisplayPanelGroup';
import StrokePanelGroup from '@components/Editor/EditorPanelGroup/StrokePanelGroup/StrokePanelGroup';
import ImagePanelGroup from '@components/Editor/EditorPanelGroup/ImagePanelGroup/ImagePanelGroup';

function ImagePanelGroupComposition(props) {
    const { fabricObject } = props;
    return (
        <>
            <FabricTransformPanelGroup fabricObject={fabricObject} />
            <DisplayPanelGroup fabricObject={fabricObject} />
            <ImagePanelGroup fabricObject={fabricObject} />
            <StrokePanelGroup fabricObject={fabricObject} />
        </>
    );
}
ImagePanelGroupComposition.propTypes = {
    fabricObject: PropTypes.object.isRequired
};

export default ImagePanelGroupComposition;
