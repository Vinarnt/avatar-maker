import React from 'react';
import PropTypes from 'prop-types';

import BorderPanelGroup from '@components/Editor/EditorPanelGroup/BorderPanelGroup/BorderPanelGroup';
import DisplayPanelGroup from '@components/Editor/EditorPanelGroup/DisplayPanelGroup/DisplayPanelGroup';

function BorderPanelGroupComposition(props) {
    const { actor } = props;
    return (
        <>
            <DisplayPanelGroup fabricObject={actor.fabricObject} />
            <BorderPanelGroup actor={actor} />
        </>
    );
}
BorderPanelGroupComposition.propTypes = {
    actor: PropTypes.object.isRequired
};

export default BorderPanelGroupComposition;
