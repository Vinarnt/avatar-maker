import React from 'react';
import PropTypes from 'prop-types';

import FabricTransformPanelGroup from '@components/Editor/EditorPanelGroup/TransformPanelGroup/FabricTransformPanelGroup/FabricTransformPanelGroup';
import DisplayPanelGroup from '@components/Editor/EditorPanelGroup/DisplayPanelGroup/DisplayPanelGroup';
import TextPanelGroup from '@components/Editor/EditorPanelGroup/TextPanelGroup/TextPanelGroup';

function TextPanelGroupComposition(props) {
    const { fabricObject } = props;
    return (
        <>
            <FabricTransformPanelGroup fabricObject={fabricObject} />
            <DisplayPanelGroup fabricObject={fabricObject} />
            <TextPanelGroup fabricObject={fabricObject} />
        </>
    );
}
TextPanelGroupComposition.propTypes = {
    fabricObject: PropTypes.object.isRequired
};

export default TextPanelGroupComposition;
