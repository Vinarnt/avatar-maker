import React from 'react';
import PropTypes from 'prop-types';

import FabricTransformPanelGroup from '@components/Editor/EditorPanelGroup/TransformPanelGroup/FabricTransformPanelGroup/FabricTransformPanelGroup';
import DisplayPanelGroup from '@components/Editor/EditorPanelGroup/DisplayPanelGroup/DisplayPanelGroup';
import ShapePanelGroup from '@components/Editor/EditorPanelGroup/ShapePanelGroup/ShapePanelGroup';
import StrokePanelGroup from '@components/Editor/EditorPanelGroup/StrokePanelGroup/StrokePanelGroup';

function ShapePanelGroupComposition(props) {
    const { fabricObject } = props;
    return (
        <>
            <FabricTransformPanelGroup fabricObject={fabricObject} />
            <DisplayPanelGroup fabricObject={fabricObject} />
            <ShapePanelGroup fabricObject={fabricObject} />
            <StrokePanelGroup fabricObject={fabricObject} />
        </>
    );
}
ShapePanelGroupComposition.propTypes = {
    fabricObject: PropTypes.object.isRequired
};

export default ShapePanelGroupComposition;
