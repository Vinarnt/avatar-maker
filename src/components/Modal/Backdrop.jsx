import React, { forwardRef } from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components';
import classNames from 'classnames';

const Backdrop = forwardRef((props, ref) => {
    const { className, backgroundColor, onClick, children } = props;

    return (
        <Container
            ref={ref}
            className={classNames(className, 'backdrop')}
            backgroundColor={backgroundColor}
            onClick={onClick}
        >
            {children}
        </Container>
    );
});
Backdrop.propTypes = {
    className: PropTypes.string,
    backgroundColor: PropTypes.string,
    onClick: PropTypes.func
};
Backdrop.defaultProps = {
    backgroundColor: '#16161eaa'
};

const Container = styled.div`
    position: absolute;
    left: 0;
    top: 0;
    right: 0;
    bottom: 0;
    background-color: ${({ backgroundColor }) => backgroundColor};
`;

export default Backdrop;
