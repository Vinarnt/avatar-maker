import React, { useRef } from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components';
import Backdrop from '@components/Modal/Backdrop';
import { getCSSColorDeclaration } from '@utils/color';

function Modal(props) {
    const { className, open, onRequestClose, header, footer, children } = props;

    const backdropRef = useRef();

    function handleBackdropClick({ target }) {
        if (backdropRef.current !== target) return;
        if (onRequestClose) onRequestClose();
    }

    return (
        <>
            {open && (
                <Backdrop
                    className={className}
                    ref={backdropRef}
                    onClick={handleBackdropClick}
                >
                    <Container className="modal-container">
                        {header && (
                            <Header className="modal-header">{header}</Header>
                        )}
                        <Body className="modal-body">{children}</Body>
                        {footer && (
                            <Footer className="modal-footer">{footer}</Footer>
                        )}
                    </Container>
                </Backdrop>
            )}
        </>
    );
}
Modal.propTypes = {
    className: PropTypes.oneOfType([PropTypes.string, PropTypes.object]),
    open: PropTypes.bool.isRequired,
    header: PropTypes.oneOfType([PropTypes.element, PropTypes.node]),
    footer: PropTypes.oneOfType([PropTypes.element, PropTypes.node]),
    onRequestClose: PropTypes.func
};

const Container = styled.div`
    display: grid;
    grid-template-areas:
        'modal-header'
        'modal-body'
        'modal-footer';
    grid-template-rows: auto 1fr auto;
    position: fixed;
    left: 0;
    top: 0;
    right: 0;
    bottom: 0;
    max-width: 100%;
    max-height: 100%;

    @media (min-width: 768px) {
        left: 50%;
        top: 50%;
        right: auto;
        bottom: auto;
        transform: translate(-50%, -50%);
        width: 768px;
        height: auto;
        min-height: 400px;
        max-height: 90%;
    }
`;

const Header = styled.div`
    grid-area: modal-header;
    grid-row: 1/1;
    padding: 5px;
    ${props =>
        getCSSColorDeclaration(
            props.theme,
            props.theme.colors.background.primary
        )};
`;

const Body = styled.div`
    grid-area: modal-body;
    grid-row: 2/2;
    overflow: auto;
    padding: 10px;
    ${props =>
        getCSSColorDeclaration(
            props.theme,
            props.theme.colors.background.primaryLight1
        )};
`;

const Footer = styled.div`
    grid-area: modal-footer;
    grid-row: 3/3;
    padding: 5px;
    ${props =>
        getCSSColorDeclaration(
            props.theme,
            props.theme.colors.background.primary
        )};
`;

export default Modal;
