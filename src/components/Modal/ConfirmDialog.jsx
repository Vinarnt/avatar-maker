import React from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components';

import Modal from '@components/Modal/Modal';
import Button from '@components/Button/Button';

function ConfirmDialog(props) {
    const {
        open,
        header,
        confirmButtonDisabled,
        onRequestClose,
        onCancel,
        onConfirm,
        children
    } = props;

    return (
        <Modal
            open={open}
            onRequestClose={onRequestClose}
            header={header}
            footer={
                <ButtonWrapper>
                    <Button onClick={onCancel}>Cancel</Button>
                    <Button
                        disabled={confirmButtonDisabled}
                        onClick={onConfirm}
                        color="primary"
                    >
                        Confirm
                    </Button>
                </ButtonWrapper>
            }
        >
            {children}
        </Modal>
    );
}
ConfirmDialog.propTypes = {
    open: PropTypes.bool.isRequired,
    confirmButtonDisabled: PropTypes.bool,
    header: PropTypes.oneOfType([PropTypes.element, PropTypes.node]),
    onRequestClose: PropTypes.func,
    onCancel: PropTypes.func.isRequired,
    onConfirm: PropTypes.func.isRequired
};
ConfirmDialog.defaultProps = {
    confirmButtonDisabled: false
};

const ButtonWrapper = styled.div`
    display: flex;
    flex-direction: row;
    justify-content: space-evenly;

    @media (min-width: 768px) {
        flex-direction: row-reverse;
        justify-content: end;
        
        & > button {
            margin-left: ${({theme}) => theme.spacings.sm};
        }
    }
`;

export default ConfirmDialog;
