import React, { useState } from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components';
import { useSelector } from 'react-redux';
import {
    SettingsContent,
    SettingsMenu,
    SettingsPage,
    SettingsPane
} from 'react-settings-pane';

import store from '@store/store';
import Modal from '@components/Modal/Modal';
import { getCSSColorDeclaration } from '@utils/color';
import { darken } from 'polished';
import { style } from '@components/Button/Button';
import AvatarPage from '@components/Setting/Page/AvatarPage';

function SettingModal(props) {
    const { open, onCancel, onConfirm } = props;
    const { dispatch } = store;

    const initialSettings = useSelector(state => state.settings);
    const setSettings = dispatch.settings.setSettings;

    const [tempSettings, setTempSettings] = useState(initialSettings);

    const menu = [
        {
            title: 'General',
            url: '/settings/general'
        },
        {
            title: 'Avatar',
            url: '/settings/avatar'
        }
    ];

    function setTempSetting(key, value) {
        setTempSettings(prevState => ({ ...prevState, [key]: value }));
    }

    function leavePaneHandler(wasSaved, newSettings, oldSettings) {
        if (wasSaved && tempSettings !== initialSettings)
            setSettings(tempSettings);
        else setTempSettings(initialSettings);

        wasSaved ? onConfirm() : onCancel();
    }

    function handleChange(key, value) {
        setTempSetting(key, value);
    }

    return (
        <ModalWrapper open={open}>
            <SettingsPaneWrapper
                items={menu}
                index="/settings/avatar"
                settings={tempSettings}
                onPaneLeave={leavePaneHandler}
            >
                <SettingsMenu headline="Settings" />
                <SettingsContent
                    header
                    saveButtonClass="btn-save"
                    closeButtonClass="btn-close"
                >
                    <SettingsPage handler="/settings/avatar">
                        <AvatarPage
                            settings={tempSettings}
                            onChange={handleChange}
                        />
                    </SettingsPage>
                </SettingsContent>
            </SettingsPaneWrapper>
        </ModalWrapper>
    );
}
SettingModal.propTypes = {
    open: PropTypes.bool.isRequired,
    onCancel: PropTypes.func.isRequired,
    onConfirm: PropTypes.func.isRequired
};

const ModalWrapper = styled(Modal)`
    .modal-body {
        padding: 0;
    }

    .btn-save {
        ${({ theme }) => style(theme, 'primary', 'sm')}
    }

    .btn-close {
        ${({ theme }) => style(theme, 'neutral', 'sm')}
    }

    @media (min-width: 768px) {
        .modal-container {
            min-width: 768px;
            max-width: 1024px;
            height: 80vh;
            width: 70vw;
        }
    }
`;

const SettingsPaneWrapper = styled(SettingsPane)`
    height: 100%;

    .settings {
        position: relative;
        flex-direction: row;
        flex: 1;
        width: 100%;
        height: 100%;

        .headline {
            display: flex;
            align-items: center;
            height: 40px;
            width: 100%;
            border-bottom: 1px solid
                ${({ theme }) => theme.colors.background.primaryLight2};
        }

        .settings-left {
            ${({ theme }) =>
                getCSSColorDeclaration(theme, theme.colors.background.primary)};
            font-weight: 600;
            min-width: 140px;
            width: 190px;
            overflow: hidden;

            .settings-menu {
                display: flex;
                flex: 1;
                flex-direction: column;
                overflow-y: auto;
                list-style: none;
                margin: 0;
                padding: 0;

                .headline {
                    justify-content: center;
                    font-weight: 700;
                    overflow: hidden;
                    white-space: normal;
                }

                .menu-item {
                    list-style: none;
                    margin: 0;
                    padding: 0;

                    a {
                        display: flex;
                        flex-direction: row;
                        text-decoration: none;
                        position: relative;
                        font-weight: 500;
                        padding: 8px 6px 10px 20px;
                        color: ${({ theme }) =>
                            darken(0.3, theme.colors.text.light)};
                        font-size: 14px;
                        line-height: 1.25em;
                        white-space: nowrap;
                        overflow: hidden;
                        text-overflow: ellipsis;
                        flex-shrink: 0;

                        &:before {
                            position: absolute;
                            top: 0;
                            left: 0;
                            height: 100%;
                            content: ' ';
                            border-left: 4px solid
                                ${({ theme }) => theme.colors.foreground.active};
                            opacity: 0;
                            filter: alpha(opacity=0);
                        }

                        &:hover:before {
                            opacity: 0.3;
                            filter: alpha(opacity=30);
                        }
                    }

                    &.active a {
                        pointer-events: none;
                        cursor: default;

                        &:before {
                            opacity: 1;
                            filter: none;
                        }
                    }

                    &.active a,
                    a:hover {
                        background: linear-gradient(
                            90deg,
                            ${({ theme }) =>
                                    theme.colors.background.primaryLight1}
                                85%,
                            transparent
                        );
                        color: ${({ theme }) => theme.colors.text.light};
                    }
                }
            }
        }

        &,
        .settings-left {
            display: flex;
        }

        .settings-content {
            flex-direction: column;
            ${({ theme }) =>
                getCSSColorDeclaration(
                    theme,
                    theme.colors.background.primaryLight2
                )};

            .settings-page {
                overflow: hidden;

                .scroller-wrap {
                    display: flex;
                    position: relative;

                    .scroller {
                        overflow-y: auto;
                    }

                    &,
                    .scroller {
                        min-height: 1px;
                        flex: 1;
                    }

                    .empty-message {
                        width: 100%;
                        ${({ theme }) =>
                            getCSSColorDeclaration(
                                theme,
                                theme.colors.background.primaryLight2
                            )};

                        p {
                            margin: 40px auto;
                            width: 100%;
                            text-align: center;
                            vertical-align: middle;
                            height: 100%;
                            font-weight: 200;
                            font-size: 150%;
                        }
                    }
                }

                .settings-innerpage {
                    display: flex;
                    flex: 1;
                    flex-direction: column;
                    justify-content: flex-start;
                    padding: ${({ theme }) => theme.spacings.md};

                    & > fieldset {
                        margin: 0;
                        padding: 0;
                        border: none;

                        & > label:first-child {
                            display: block;
                            margin-bottom: ${({ theme }) => theme.spacings.sm};
                            border-bottom: 1px solid
                                ${({ theme }) => theme.colors.text.light};
                            font-weight: bold;
                        }
                    }
                }
            }

            .headline {
                ${({ theme }) =>
                    getCSSColorDeclaration(
                        theme,
                        theme.colors.background.primaryLight1
                    )};
                padding-left: ${({ theme }) => theme.spacings.sm};
                border-bottom: 1px solid
                    ${({ theme }) => theme.colors.background.primaryLight2};
                overflow: hidden;
            }
        }

        .settings-page,
        .settings-content {
            display: flex;
            flex: 1;
        }

        .settings-footer {
            display: flex;
            flex-direction: row-reverse;
            padding: 5px;
            ${({ theme }) =>
                getCSSColorDeclaration(
                    theme,
                    theme.colors.background.primaryLight1
                )};
        }
    }
`;

export default SettingModal;
