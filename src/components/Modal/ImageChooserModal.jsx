import React, { useRef, useState } from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components';
import classNames from 'classnames';

import RingSpinner from '@components/ProgressBar/Spinner/RingSpinner';
import ConfirmDialog from '@components/Modal/ConfirmDialog';
import { loadImageFromFile } from '@utils/image';

function ImageChooserModal(props) {
    const { open, onRequestClose, onCancel, onConfirm } = props;

    const [isLoading, setLoading] = useState(false);
    const [imageName, setImageName] = useState();
    const [imageURL, setImageURL] = useState();

    const fileInputRef = useRef();

    async function handleFileChange() {
        const file = fileInputRef.current.files[0];

        setLoading(true);
        setImageName(file.name);

        const imageDataURL = await loadImageFromFile(file);
        setImageURL(imageDataURL);
    }

    function handleImageLoad() {
        setLoading(false);
    }

    function handleRequestClose() {
        onRequestClose();
        reset();
    }

    function handleCancel() {
        onCancel();
        reset();
    }

    function handleConfirm() {
        onConfirm(imageName, imageURL);
        reset();
    }

    function reset() {
        setLoading(false);
        setImageName(undefined);
        setImageURL(undefined);
    }

    return (
        <ConfirmDialog
            open={open}
            onRequestClose={handleRequestClose}
            onCancel={handleCancel}
            onConfirm={handleConfirm}
            confirmButtonDisabled={isLoading}
            header={<div className="text-center">Replace image</div>}
        >
            <FileInput ref={fileInputRef} onChange={handleFileChange} />

            <ImagePreview>
                {imageURL && (
                    <>
                        <ImageTitle>{imageName}</ImageTitle>
                        <img
                            className={classNames({
                                hidden: isLoading
                            })}
                            alt="preview"
                            src={imageURL}
                            onLoad={handleImageLoad}
                        />
                    </>
                )}
                {isLoading && <RingSpinner />}
            </ImagePreview>
        </ConfirmDialog>
    );
}
ImageChooserModal.propTypes = {
    open: PropTypes.bool.isRequired,
    onRequestClose: PropTypes.func.isRequired,
    onCancel: PropTypes.func.isRequired,
    onConfirm: PropTypes.func.isRequired
};

const FileInput = styled.input.attrs(props => ({
    type: 'file',
    accept: '.jpg, .jpeg, .png, .svg, .gif'
}))``;

const ImageTitle = styled.div`
    font-weight: bold;
`;

const ImagePreview = styled.div`
    max-width: 100%;
    text-align: center;

    & > img {
        object-fit: contain;
        max-width: 100%;
    }

    @media (min-width: 768px) {
        & > img {
            max-width: 500px;
        }
    }
`;

export default ImageChooserModal;
