import React, { useEffect, useRef } from 'react';
import styled from 'styled-components';
import { MenuProvider } from 'react-contexify';

import SceneManager from '@services/SceneManager';
import CanvasContextMenu from '@components/ContextMenu/CanvasContextMenu/CanvasContextMenu';

const Canvas = function Canvas() {
    const canvasRef = useRef(null);

    useEffect(() => {
        SceneManager.init(canvasRef);
        return () => {
            SceneManager.cleanup();
        };
    }, []);

    return (
        <>
            <Wrapper id={CanvasContextMenu.TYPE} storeRef={false}>
                <canvas ref={canvasRef} />
            </Wrapper>
            <CanvasContextMenu />
        </>
    );
};

const Wrapper = styled(MenuProvider)`
    height: 100%;
    overflow: hidden;
`;

export default Canvas;
