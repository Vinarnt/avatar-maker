import React from 'react';
import styled from 'styled-components';
import { useSelector } from 'react-redux';

import Canvas from '@components/Workplace/Canvas/Canvas';
import AutoHidingToolbar from '@components/AutoHidingToolbar/AutoHidingToolbar';
import ScaleLockButton from '@components/Button/AutoHidingToolbar/ScaleLockButton';
import CenterScalingButton from '@components/Button/AutoHidingToolbar/CenterScalingButton';
import { Desktop } from '@utils/mediaQuery';

function Workplace (props) {
    const isSceneInitialized = useSelector(state => state.scene.isInitialized);
    return (
        <WorkplaceWrapper {...props}>
            <Canvas />

            {isSceneInitialized && (
                <>
                    <Desktop>
                        <AutoHidingToolbar position={'top'}>
                            <ScaleLockButton />
                            <CenterScalingButton />
                        </AutoHidingToolbar>
                    </Desktop>
                </>
            )}
        </WorkplaceWrapper>
    );
}

const WorkplaceWrapper = styled.div`
    position: relative;
    height: 100%;
    overflow: hidden;
`;

export default Workplace;
