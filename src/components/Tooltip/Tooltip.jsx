import styled from 'styled-components';
import Tooltip from 'react-tooltip-lite';

import { getCSSColorDeclaration } from '@utils/color';

const TooltipWrapper = styled(Tooltip)`
    .react-tooltip-lite {
        ${({ theme }) =>
            getCSSColorDeclaration(theme, theme.colors.background.primary)};
    }

    .react-tooltip-lite-arrow {
        border-color: ${({ theme }) => theme.colors.background.primary};
    }

    .react-tooltip-lite,
    .react-tooltip-lite-arrow {
        filter: opacity(0.95);
    }
`;

export default TooltipWrapper;
