import React, { useEffect, useRef, useState } from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components';
import classNames from 'classnames';
import { rgba } from 'polished';
import { SketchPicker } from 'react-color';
import { fabric } from 'fabric';

function ColorInput({ value, onChange }) {
    const [isPickerOpen, setPickerOpen] = useState(false);
    const colorPickerWrapperRef = useRef();

    let color = value;
    if (typeof color === 'string') {
        color = new fabric.Color(color).toRgbaObject();
    }

    useEffect(() => {
        function closeColorPicker() {
            if (isPickerOpen) setPickerOpen(false);
        }

        const onWindowClick = event => {
            const target = event.target;
            if (
                target !== colorPickerWrapperRef?.current &&
                !colorPickerWrapperRef.current?.contains(target)
            ) {
                closeColorPicker();
            }
        };
        const onKeyDown = ({ key }) => {
            switch (key) {
                case 'Escape':
                    closeColorPicker();
                    break;
                default:
            }
        };
        window.addEventListener('keydown', onKeyDown);
        window.addEventListener('click', onWindowClick);

        return () => {
            window.removeEventListener('keydown', onKeyDown);
            window.removeEventListener('click', onWindowClick);
        };
    }, [isPickerOpen]);

    function getLabel(color) {
        if (color.a === 0) return 'transparent';
        else return '#' + fabric.Color.fromRgbaObject(color).toHexa();
    }

    function handleChange(color) {
        onChange(color);
    }

    function handleClick() {
        setPickerOpen(!isPickerOpen);
    }

    return (
        <div>
            <SketchPickerWrapper
                ref={colorPickerWrapperRef}
                className={classNames({
                    hidden: !isPickerOpen,
                    left: true
                })}
            >
                <SketchPicker color={color} onChange={handleChange} />
            </SketchPickerWrapper>
            <Wrapper onClick={handleClick}>
                <ColorPreview color={color} />
                <ColorLabel>{getLabel(color)}</ColorLabel>
            </Wrapper>
        </div>
    );
}

ColorInput.propTypes = {
    value: PropTypes.oneOfType([PropTypes.object, PropTypes.string]),
    onChange: PropTypes.func.isRequired
};
ColorInput.defaultProps = {
    value: { r: 0, g: 0, b: 0, a: 0 }
};

const Wrapper = styled.div`
    display: inline-block;
`;

const SketchPickerWrapper = styled.div`
    position: fixed;

    &.hidden {
        display: none;
    }

    &.left {
        transform: translate(-100%, -100%);
    }
`;

const ColorPreview = styled.div.attrs(({ color }) => ({
    style: {
        backgroundColor: rgba(color.r, color.g, color.b, color.a)
    }
}))`
    display: inline-block;
    width: 35px;
    height: 20px;
    border: 1px solid ${({ theme }) => theme.colors.background.primary};
    cursor: pointer;
`;

const ColorLabel = styled.div`
    display: inline-block;
    line-height: 20px;
    vertical-align: top;
    margin-left: 5px;
`;

export default ColorInput;
