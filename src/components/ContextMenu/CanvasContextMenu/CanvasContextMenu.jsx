import React from 'react';
import { Menu } from 'react-contexify';

import '../ContextMenu.scss';
import CanvasAddActorSubMenu from '@components/ContextMenu/Menu/AddActorSubMenu/CanvasAddActorSubMenu';

function CanvasContextMenu() {
    return (
        <Menu id={CanvasContextMenu.TYPE}>
            <CanvasAddActorSubMenu />
        </Menu>
    );
}
CanvasContextMenu.TYPE = 'canvas-context-menu';

export default CanvasContextMenu;
