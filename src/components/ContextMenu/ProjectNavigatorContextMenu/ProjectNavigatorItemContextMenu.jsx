import React from 'react';
import { Menu, Separator } from 'react-contexify';

import '../ContextMenu.scss';
import AddActorSubMenu from '../Menu/AddActorSubMenu/AddActorSubMenu';
import RemoveActorMenuItem from '@components/ContextMenu/Menu/RemoveActorMenuItem/RemoveActorMenuItem';
import ZIndexMenuGroup from '@components/ContextMenu/Menu/Group/ZIndexMenuGroup/ZIndexMenuGroup';

function ProjectNavigatorItemContextMenu() {
    return (
        <Menu id={ProjectNavigatorItemContextMenu.TYPE}>
            <AddActorSubMenu />
            <Separator />
            <RemoveActorMenuItem />
            <ZIndexMenuGroup />
        </Menu>
    );
}
ProjectNavigatorItemContextMenu.TYPE = 'project-navigator-item-context-menu';

export default ProjectNavigatorItemContextMenu;
