import React from 'react';
import PropType from 'prop-types';
import { Menu } from 'react-contexify';

import AddActorMenuContent from '@components/ContextMenu/Menu/AddActorSubMenu/AddActorMenuContent';

function ToolbarAddActorContextMenu(props) {
    return (
        <Menu id={ToolbarAddActorContextMenu.TYPE}>
            <AddActorMenuContent {...props} />
        </Menu>
    );
}
ToolbarAddActorContextMenu.propTypes = {
    forwardedRef: PropType.object
};
ToolbarAddActorContextMenu.TYPE = 'project-navigator-toolbar-context-menu';

export default ToolbarAddActorContextMenu;
