import React from 'react';
import { Menu } from 'react-contexify';

import '../ContextMenu.scss';
import AddActorSubMenu from '../Menu/AddActorSubMenu/AddActorSubMenu';

function ProjectNavigatorContextMenu() {
    return (
        <Menu id={ProjectNavigatorContextMenu.TYPE}>
            <AddActorSubMenu />
        </Menu>
    );
}
ProjectNavigatorContextMenu.TYPE = 'project-navigator-context-menu';

export default ProjectNavigatorContextMenu;
