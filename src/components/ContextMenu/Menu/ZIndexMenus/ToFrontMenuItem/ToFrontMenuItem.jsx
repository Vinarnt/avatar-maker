import React from 'react';
import ZIndexMenuItem from '@components/ContextMenu/Menu/ZIndexMenus/ZIndexMenuItem';

function ToFrontMenuItem(props) {
    return (
        <ZIndexMenuItem stackingFunctionName={'bringToFront'} {...props}>
            Bring to front
        </ZIndexMenuItem>
    );
}

export default ToFrontMenuItem;
