import React from 'react';
import ZIndexMenuItem from '@components/ContextMenu/Menu/ZIndexMenus/ZIndexMenuItem';

function ToBackMenuItem(props) {
    return (
        <ZIndexMenuItem stackingFunctionName={'sendToBack'} {...props}>
            Send to back
        </ZIndexMenuItem>
    );
}

export default ToBackMenuItem;
