import React from 'react';
import ZIndexMenuItem from '@components/ContextMenu/Menu/ZIndexMenus/ZIndexMenuItem';

function BringForwardMenuItem(props) {
    return (
        <ZIndexMenuItem stackingFunctionName={'bringForward'} {...props}>
            Bring forward
        </ZIndexMenuItem>
    );
}

export default BringForwardMenuItem;
