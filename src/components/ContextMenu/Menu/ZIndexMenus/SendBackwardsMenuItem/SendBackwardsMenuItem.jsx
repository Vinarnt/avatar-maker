import React from 'react';
import ZIndexMenuItem from '@components/ContextMenu/Menu/ZIndexMenus/ZIndexMenuItem';

function SendBackwardsMenuItem(props) {
    return (
        <ZIndexMenuItem stackingFunctionName={'sendBackwards'} {...props}>
            Send backwards
        </ZIndexMenuItem>
    );
}

export default SendBackwardsMenuItem;
