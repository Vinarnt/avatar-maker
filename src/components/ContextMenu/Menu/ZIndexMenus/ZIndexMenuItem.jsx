import React from 'react';
import PropTypes from 'prop-types';
import { useStore } from 'react-redux';
import { Item } from 'react-contexify';
import SceneManager from '@services/SceneManager';

function ZIndexMenuItem(props) {
    const { stackingFunctionName, children, ...rest } = props;
    const { select, getState } = useStore();

    function handleClick({ event, props: { actor } }) {
        if (select.scene.isSelected(actor)(getState()))
            SceneManager.canvas.getActiveObject()[stackingFunctionName]();
        else actor.fabricObject[stackingFunctionName]();
    }

    return (
        <Item onClick={handleClick} {...rest}>
            {children}
        </Item>
    );
}
ZIndexMenuItem.propTypes = {
    stackingFunctionName: PropTypes.string.isRequired,
    children: PropTypes.string.isRequired
};

export default ZIndexMenuItem;
