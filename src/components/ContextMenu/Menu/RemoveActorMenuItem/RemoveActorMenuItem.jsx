import React from 'react';
import { useSelector } from 'react-redux';
import { Item } from 'react-contexify';

import SceneManager from '@services/SceneManager';

function RemoveActorMenuItem(props) {
    const selectedActors = useSelector(state => state.scene.selectedActors);

    function handleClick({ event, props: { actor } }) {
        if (selectedActors.includes(actor))
            SceneManager.removeActors(selectedActors.slice());
        else SceneManager.removeActor(actor);
    }

    return (
        <Item onClick={handleClick} {...props}>
            Remove
        </Item>
    );
}

export default RemoveActorMenuItem;
