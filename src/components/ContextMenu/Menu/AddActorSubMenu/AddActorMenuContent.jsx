import React from 'react';
import PropTypes from 'prop-types';
import { Item, Separator, Submenu } from 'react-contexify';

import SceneManager from '@services/SceneManager';
import ActorFactory from '@factories/ActorFactory';

function AddActorMenuContent(props) {
    const { x, y } = props;

    function handleItemClick({ event, props: { type } }) {
        let actor;
        switch (type) {
            case 'image':
                const fileInput = document.createElement('input');
                fileInput.type = 'file';
                fileInput.click();
                fileInput.onchange = ({ target: { files } }) => {
                    const file = files[0];
                    SceneManager.addActor(
                        ActorFactory.createImage(file.name, file)
                    );
                };
                break;
            case 'text':
                actor = SceneManager.addActor(
                    ActorFactory.createText('', x, y)
                );
                actor.fabricObject.enterEditing();
                break;
            case 'rectangle':
                actor = SceneManager.addActor(
                    ActorFactory.createShape('rectangle', 'white', x, y)
                );
                break;
            case 'triangle':
                actor = SceneManager.addActor(
                    ActorFactory.createShape('triangle', 'white', x, y)
                );
                break;
            case 'circle':
                actor = SceneManager.addActor(
                    ActorFactory.createShape('circle', 'white', x, y)
                );
                break;
            case 'ellipse':
                actor = SceneManager.addActor(
                    ActorFactory.createShape('ellipse', 'white', x, y)
                );
                break;
            case 'border':
                actor = SceneManager.addActor(ActorFactory.createBorder());
                break;
            default:
                console.error('Type not handled', type);
                return;
        }

        if (actor) SceneManager.selectActor(actor);
    }

    return (
        <>
            <Item data={{ type: 'image' }} onClick={handleItemClick}>
                Image
            </Item>
            <Item data={{ type: 'text' }} onClick={handleItemClick}>
                Text
            </Item>
            <Submenu label="Shape">
                <Item data={{ type: 'rectangle' }} onClick={handleItemClick}>
                    Rectangle
                </Item>
                <Item data={{ type: 'triangle' }} onClick={handleItemClick}>
                    Triangle
                </Item>
                <Item data={{ type: 'circle' }} onClick={handleItemClick}>
                    Circle
                </Item>
                <Item data={{ type: 'ellipse' }} onClick={handleItemClick}>
                    Ellipse
                </Item>
            </Submenu>
            <Separator />
            <Item data={{ type: 'border' }} onClick={handleItemClick}>
                Border
            </Item>
        </>
    );
}
AddActorMenuContent.propTypes = {
    x: PropTypes.number,
    y: PropTypes.number
};

export default AddActorMenuContent;
