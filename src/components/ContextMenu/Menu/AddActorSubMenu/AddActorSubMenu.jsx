import React from 'react';
import PropTypes from 'prop-types';
import { Submenu } from 'react-contexify';

import AddActorMenuContent from '@components/ContextMenu/Menu/AddActorSubMenu/AddActorMenuContent';

function AddActorSubMenu(props) {
    return (
        <Submenu label="Add">
            <AddActorMenuContent {...props} />
        </Submenu>
    );
}
AddActorSubMenu.propTypes = {
    x: PropTypes.number,
    y: PropTypes.number
};

export default AddActorSubMenu;
