import React from 'react';
import PropTypes from 'prop-types';
import AddActorSubMenu from '@components/ContextMenu/Menu/AddActorSubMenu/AddActorSubMenu';
import SceneManager from '@services/SceneManager';

function CanvasAddActorSubMenu(props) {
    const {
        nativeEvent: { clientX, clientY }
    } = props;

    const absolutePosition = SceneManager.canvas.projectViewport(
        clientX,
        clientY
    );
    const childProps = { ...absolutePosition };

    return <AddActorSubMenu {...childProps} />;
}
CanvasAddActorSubMenu.propTypes = {
    nativeEvent: PropTypes.object
};

export default CanvasAddActorSubMenu;
