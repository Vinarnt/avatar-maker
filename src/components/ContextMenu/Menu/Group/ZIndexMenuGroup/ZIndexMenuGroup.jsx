import React from 'react';
import { Separator } from 'react-contexify';
import ToFrontMenuItem from '@components/ContextMenu/Menu/ZIndexMenus/ToFrontMenuItem/ToFrontMenuItem';
import BringForwardMenuItem from '@components/ContextMenu/Menu/ZIndexMenus/BringForwardMenuItem/BringForwardMenuItem';
import ToBackMenuItem from '@components/ContextMenu/Menu/ZIndexMenus/ToBackMenuItem/ToBackMenuItem';
import SendBackwardsMenuItem from '@components/ContextMenu/Menu/ZIndexMenus/SendBackwardsMenuItem/SendBackwardsMenuItem';
import { useSelector, useStore } from 'react-redux';

function ZIndexMenuGroup(props) {
    const { select } = useStore();
    const selectionCount = useSelector(state =>
        select.scene.selectionCount(state)
    );

    return (
        <>
            <Separator />
            <ToFrontMenuItem {...props} />
            {selectionCount <= 1 && <BringForwardMenuItem {...props} />}
            <ToBackMenuItem {...props} />
            {selectionCount <= 1 && <SendBackwardsMenuItem {...props} />}
        </>
    );
}

export default ZIndexMenuGroup;
