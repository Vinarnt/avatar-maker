import React, { useState } from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components';
import classNames from 'classnames';
import { getCSSColorDeclaration } from '@utils/color';

function ToggleButton(props) {
    const { active, onChange, children } = props;

    const [isActive, setActive] = useState(active);

    function handleClick() {
        const newState = !isActive;
        setActive(newState);

        onChange(newState);
    }

    return (
        <Wrapper
            className={classNames(
                {
                    'btn-active': isActive
                },
                props.className
            )}
            onClick={handleClick}
        >
            {children}
        </Wrapper>
    );
}
ToggleButton.propTypes = {
    className: PropTypes.string,
    active: PropTypes.bool,
    onChange: PropTypes.func.isRequired
};
ToggleButton.defaultProps = {
    active: false
};

const Wrapper = styled.button`
    padding: 2px 5px;
    border: 1px solid transparent;
    ${({ theme }) =>
        getCSSColorDeclaration(theme, theme.colors.background.primary)};
    cursor: pointer;

    :hover {
        filter: brightness(80%);
    }

    &.btn-active {
        ${({ theme }) =>
            getCSSColorDeclaration(
                theme,
                theme.colors.background.primaryDark1
            )};
        border: 1px solid white;
    }
`;

export default ToggleButton;
