import React from 'react';
import styled from 'styled-components';
import { FaGitlab, FaExternalLinkAlt } from 'react-icons/fa';

import Button from '@components/Button/Button';

function GitlabButton() {
    return (
        <a
            href="https://gitlab.com/Vinarnt/avatar-maker"
            target="_blank"
            rel="noopener noreferrer"
        >
            <ButtonWrapper size="md" color="info">
                <FaGitlab />
                <span>Gitlab</span>
                <FaExternalLinkAlt />
            </ButtonWrapper>
        </a>
    );
}

const ButtonWrapper = styled(Button)`
    span {
        margin: 0 ${({ theme }) => theme.spacings.xs};
    }

    & > * {
        vertical-align: middle;
    }
`;

export default GitlabButton;
