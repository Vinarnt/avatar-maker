import React from 'react';
import { FaFileDownload } from 'react-icons/fa';
import Button from '@components/Button/Button';
import Tooltip from '@components/Tooltip/Tooltip';
import FileDownloader from '@services/FileDownloader';

function SaveButton() {
    function handleClick() {
        FileDownloader.downloadCanvasImage();
    }

    return (
        <>
            <Tooltip content="Download image" direction="down">
                <Button onClick={handleClick}>
                    <FaFileDownload />
                </Button>
            </Tooltip>
        </>
    );
}

export default SaveButton;
