import React, { useState } from 'react';
import { FaCog } from 'react-icons/fa';

import Button from '@components/Button/Button';
import SettingModal from '@components/Modal/SettingModal';
import Tooltip from '@components/Tooltip/Tooltip';

function SettingButton() {
    const [isOpen, setOpen] = useState(false);

    function handleClick() {
        setOpen(true);
    }

    function handleCancel() {
        setOpen(false);
    }

    function handleConfirm() {
        setOpen(false);
    }

    return (
        <>
            <Tooltip content="Settings" direction="down">
                <Button onClick={handleClick}>
                    <FaCog />
                </Button>
            </Tooltip>
            <SettingModal
                open={isOpen}
                onCancel={handleCancel}
                onConfirm={handleConfirm}
            />
        </>
    );
}

export default SettingButton;
