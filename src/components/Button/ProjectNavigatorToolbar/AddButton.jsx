import React from 'react';
import { contextMenu } from 'react-contexify';
import { FaPlus } from 'react-icons/fa';

import ToolbarAddActorContextMenu from '@components/ContextMenu/ProjectNavigatorContextMenu/ToolbarAddActorContextMenu';
import Button from '@components/Button/Button';
import Tooltip from '@components/Tooltip/Tooltip';

function AddButton() {
    function handleClick(e) {
        e.stopPropagation();

        const { x, y, height } = e.currentTarget.getBoundingClientRect();
        const event = new MouseEvent('click', {
            clientX: x,
            clientY: y + height
        });

        contextMenu.show({
            id: ToolbarAddActorContextMenu.TYPE,
            event,
            props: {
                x: x,
                y: y + height
            }
        });
    }

    return (
        <>
            <Tooltip content="Add" direction="down" zIndex={10001}>
                <Button onClick={handleClick}>
                    <FaPlus />
                </Button>
            </Tooltip>
            <ToolbarAddActorContextMenu />
        </>
    );
}

export default AddButton;
