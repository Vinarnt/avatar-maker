import React from 'react';
import { FaTrash } from 'react-icons/fa';

import Button from '@components/Button/Button';
import useScene from '@hooks/useScene';
import Tooltip from '@components/Tooltip/Tooltip';

function RemoveButton() {
    const { removeSelectedActors } = useScene();

    function handleClick() {
        removeSelectedActors();
    }

    return (
        <Tooltip content="Remove" direction="down">
            <Button onClick={handleClick}>
                <FaTrash />
            </Button>
        </Tooltip>
    );
}

export default RemoveButton;
