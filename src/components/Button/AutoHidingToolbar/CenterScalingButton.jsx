import React, { useState } from 'react';
import { GiResize } from 'react-icons/gi';
import { MdSettingsOverscan } from 'react-icons/md';

import ToggleButton from '@components/Button/AutoHidingToolbar/ToggleButton';
import SceneManager from '@services/SceneManager';
import Tooltip from '@components/Tooltip/Tooltip';

function CenterScalingButton() {
    const [isCentered, setCentered] = useState(
        SceneManager.canvas.centeredScaling
    );

    return (
        <Tooltip
            content={isCentered ? 'Scale from origin' : 'Scale from center'}
            direction="bottom"
        >
            <ToggleButton
                active={isCentered}
                onChange={state => {
                    SceneManager.canvas.set({ centeredScaling: state });
                    setCentered(state);
                }}
            >
                {isCentered && <MdSettingsOverscan />}
                {!isCentered && <GiResize />}
            </ToggleButton>
        </Tooltip>
    );
}

export default CenterScalingButton;
