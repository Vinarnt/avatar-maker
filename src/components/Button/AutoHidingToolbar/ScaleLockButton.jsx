import React, { useState } from 'react';
import { FaLock, FaLockOpen } from 'react-icons/fa';

import ToggleButton from '@components/Button/AutoHidingToolbar/ToggleButton';
import SceneManager from '@services/SceneManager';
import Tooltip from '@components/Tooltip/Tooltip';

function ScaleLockButton() {
    const [isLocked, setLocked] = useState(!SceneManager.canvas.uniformScaling);

    return (
        <Tooltip
            content={isLocked ? 'Unlock scale ratio' : 'Lock scale ratio'}
            direction="bottom"
        >
            <ToggleButton
                active={isLocked}
                onChange={state => {
                    const canvas = SceneManager.canvas;
                    canvas.uniformScaling = !state;
                    canvas.getObjects().forEach(object => {
                        object.setNonUniScaleControlsVisibility(!state);
                    });
                    SceneManager.requestRenderAll();
                    setLocked(state);
                }}
            >
                {isLocked && <FaLock />}
                {!isLocked && <FaLockOpen />}
            </ToggleButton>
        </Tooltip>
    );
}

export default ScaleLockButton;
