import React from 'react';
import styled from 'styled-components';

import InnerButton from '@components/Button/ToggleButton';

function ToggleButton(props) {
    return <Wrapper {...props} />;
}

const Wrapper = styled(InnerButton)`
    background-color: inherit;
    padding: 10px;

    &.btn-active {
        background-color: inherit;
        border: none;
    }
`;

export default ToggleButton;
