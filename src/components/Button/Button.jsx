import React from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components';

import { getCSSColorDeclaration } from '@utils/color';
import { darken, desaturate } from 'polished';

function Button(props) {
    return <ButtonWrapper {...props}>{props.children}</ButtonWrapper>;
}
Button.propTypes = {
    className: PropTypes.oneOfType([PropTypes.string, PropTypes.object]),
    color: PropTypes.string,
    size: PropTypes.string,
    disabled: PropTypes.bool,
    onClick: PropTypes.func
};
Button.defaultProps = {
    color: 'neutral',
    size: 'sm'
};

export const style = (theme, color, size) => `
    border: solid 1px transparent;
    font-weight: bold;
    cursor: pointer;
    padding: 2px 5px;
    border-radius: 3px;

     ${getCSSDeclaration(theme, color)};
     font-size: ${getSize(theme, size)}

     &:hover, &:focus {
        ${getCSSDeclaration(theme, color, 'hover')};
        ${color === 'neutral' &&
            `
                opacity: 0.7;
                filter: alpha(opacity=70);
        `}
     } 
     
      &:disabled {
        ${getCSSDeclaration(theme, color, 'disabled')};
      }`;

const ButtonWrapper = styled.button`
    ${({ theme, color, size }) => style(theme, color, size)}
`;

function getSize(theme, size) {
    switch (size) {
        case 'xs':
        case 'sm':
        case 'md':
        case 'lg':
        case 'xl':
        case 'xxl':
            return theme.sizes[size];
        default:
            return size;
    }
}

function getColor(theme, color) {
    switch (color) {
        case 'primary':
        case 'info':
        case 'danger':
        case 'warning':
        case 'success':
        case 'neutral':
            return theme.colors.foreground[color];
        default:
            return color;
    }
}

function getCSSDeclaration(theme, color, pseudoClass) {
    const resolvedColor = getColor(theme, color);
    let colorVariant;
    switch (pseudoClass) {
        case 'hover':
            colorVariant = darken(0.05, resolvedColor);
            break;
        case 'disabled':
            colorVariant = desaturate(0.5, resolvedColor);
            break;
        default:
            colorVariant = resolvedColor;
            break;
    }
    return getCSSColorDeclaration(theme, colorVariant);
}

export default Button;
