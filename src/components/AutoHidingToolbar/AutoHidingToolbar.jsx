import React from 'react';
import PropTypes from 'prop-types';
import styled, { css } from 'styled-components';
import {
    FaArrowCircleDown,
    FaArrowCircleLeft,
    FaArrowCircleRight,
    FaArrowCircleUp
} from 'react-icons/fa';

const minLength = '150px';
const minWidth = '50px';
const transitionDuration = '0.25s';

function AutoHidingToolbar(props) {
    function getMarkerIcon(position) {
        switch (position) {
            case 'left':
                return <FaArrowCircleRight />;
            case 'top':
                return <FaArrowCircleDown />;
            case 'right':
                return <FaArrowCircleLeft />;
            case 'bottom':
                return <FaArrowCircleUp />;
            default:
        }
    }

    return (
        <Sensor {...props}>
            <Container {...props}>{props.children}</Container>
            <Marker {...props}>{getMarkerIcon(props.position)}</Marker>
        </Sensor>
    );
}
AutoHidingToolbar.propTypes = {
    className: PropTypes.string,
    position: PropTypes.oneOf(['left', 'top', 'right', 'bottom'])
};

function getSensorCSSForPosition(position) {
    switch (position) {
        case 'left':
            return css`
                min-width: ${minWidth};
                min-height: ${minLength};
                max-height: 80%;
                left: 0;
                top: 50%;
                transform: translateY(-50%);
            `;
        case 'top':
            return css`
                min-width: ${minLength};
                max-width: 80%;
                min-height: ${minWidth};
                left: 50%;
                top: 0;
                transform: translateX(-50%);
            `;
        case 'right':
            return css`
                min-width: ${minWidth};
                min-height: ${minLength};
                max-height: 80%;
                right: 0;
                top: 50%;
                transform: translateY(-50%);
            `;
        case 'bottom':
            return css`
                min-width: ${minLength};
                max-width: 80%;
                min-height: ${minWidth};
                left: 50%;
                bottom: 0;
                transform: translateX(-50%);
            `;
        default:
    }
}

function getWrapperCSSForPosition(position) {
    switch (position) {
        case 'left':
            return css`
                flex-direction: column;
                min-height: ${minLength};
                transition: transform ${transitionDuration};
                transform: translateX(-100%);
                border-right: 1px solid ${({theme}) => theme.colors.background.primaryLight3};
            `;
        case 'top':
            return css`
                flex-direction: row;
                min-width: ${minLength};
                transition: transform ${transitionDuration};
                transform: translateY(-100%);
                border-bottom: 1px solid ${({theme}) => theme.colors.background.primaryLight3};
            `;
        case 'right':
            return css`
                flex-direction: column;
                min-height: ${minLength};
                transition: transform ${transitionDuration};
                transform: translateX(100%);
            `;
        case 'bottom':
            return css`
                flex-direction: row;
                min-width: ${minLength};
                transition: transform ${transitionDuration};
                transform: translateY(100%);
            `;
        default:
    }
}

function getWrapperTransitionCSSForPosition(position) {
    switch (position) {
        case 'left':
            return css`
                transform: translateX(0);
            `;
        case 'top':
            return css`
                transform: translateY(0%);
            `;
        case 'right':
            return css`
                transform: translateX(0%);
            `;
        case 'bottom':
            return css`
                transform: translateY(0%);
            `;
        default:
    }
}

const Sensor = styled.div`
    position: absolute;
    display: flex;
    align-items: center;
    justify-content: center;
    ${({ position }) => getSensorCSSForPosition(position)};
`;

const Container = styled.div`
    position: relative;
    ${({ position }) => getWrapperCSSForPosition(position)};
    display: flex;
    justify-content: center;
    align-items: center;
    width: 100%;
    height: 100%;
    padding: ${({theme}) => theme.spacings.xs};
    overflow: auto;

    ${Sensor}:hover & {
        ${({ position }) => getWrapperTransitionCSSForPosition(position)};
    }

    & > * {
        margin: ${({theme}) => theme.spacings.xs};
    }
`;

const Marker = styled.div`
    position: absolute;
    transition: opacity ${transitionDuration};

    ${Sensor}:hover & {
        opacity: 0;
        pointer-events: none;
    }
`;

export default AutoHidingToolbar;
