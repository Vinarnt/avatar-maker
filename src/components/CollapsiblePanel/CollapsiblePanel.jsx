import React from 'react';
import PropTypes from 'prop-types';
import classNames from 'classnames';
import styled from 'styled-components';
import { FaCaretDown } from 'react-icons/fa';
import { connect } from 'react-redux';
import store from '@store/store';

const { select } = store;

function CollapsiblePanel(props) {
    const { forwardRef, identifier, open, header, children, onChange } = props;
    const { dispatch } = store;

    function handleChange() {
        dispatch.ui.toggleCollapsibleState(identifier);
        if (onChange) onChange();
    }

    return (
        <Wrapper ref={forwardRef} {...props} className={props.className}>
            <Header className="collapsible-panel-header" onClick={handleChange}>
                {header}
                <Icon
                    className={classNames({
                        'collapsible-panel-header-icon': true,
                        open: open
                    })}
                >
                    <FaCaretDown />
                </Icon>
            </Header>
            <Content
                className={classNames({
                    'collapsible-panel-content': true,
                    open: open
                })}
            >
                {children}
            </Content>
        </Wrapper>
    );
}
CollapsiblePanel.propTypes = {
    forwardRef: PropTypes.object,
    identifier: PropTypes.string.isRequired,
    className: PropTypes.string,
    open: PropTypes.bool,
    header: PropTypes.oneOfType([PropTypes.string, PropTypes.node]),
    onChange: PropTypes.func
};

const Wrapper = styled.div`
    overflow: hidden;
`;
const Header = styled.div`
    cursor: pointer;
    user-select: none;
`;
const Icon = styled.div`
    transition: transform 0.25s;
    float: right;

    &.open {
        transform: rotateZ(180deg);
    }
`;
const Content = styled.div`
    height: 0;

    &.open {
        height: initial;
    }
`;

const mapStateToProps = (state, props) => ({
    open: select.ui.getCollapsibleState(props.identifier)(state)
});

export default connect(mapStateToProps)(CollapsiblePanel);
