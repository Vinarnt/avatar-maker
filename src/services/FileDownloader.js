import SceneManager from '@services/SceneManager';
import store from '@store/store';
import settingIdentifiers from '@constants/settings';

const FileDownloader = () => {
    const downloadCanvasImage = () => {
        const canvas = SceneManager.canvas;
        const settings = store.getState().settings;
        const imageShape = settings[settingIdentifiers.general.image.shape];
        const imageSize = settings[settingIdentifiers.general.image.size];
        const isCircle = imageShape === 'circle';
        const imageWidth = isCircle ? imageSize.radius * 2 : imageSize.width;
        const imageHeight = isCircle ? imageSize.radius * 2 : imageSize.height;

        const backgroundColor = canvas.backgroundColor;
        canvas.set('backgroundColor', undefined);
        const link = document.createElement('a');
        link.href = canvas.toDataURL({
            format: 'png',
            left: canvas.getWidth() * 0.5 - imageWidth * 0.5,
            top: canvas.getHeight() * 0.5 - imageHeight * 0.5,
            width: imageWidth,
            height: imageHeight
        });
        link.download = 'image.png';
        link.style.display = 'none';
        document.body.appendChild(link);
        link.click();
        document.body.removeChild(link);
        canvas.set('backgroundColor', backgroundColor);
    };

    return {
        downloadCanvasImage: downloadCanvasImage
    };
};

const instance = FileDownloader();
window.downloadImage = () => instance.downloadCanvasImage(SceneManager.canvas);

export default instance;
