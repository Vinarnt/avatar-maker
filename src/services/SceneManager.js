import store from '@store/store';
import { fabric } from 'fabric';
import _function from 'lodash/function';

import { _initFabricObject } from '@utils/actor';
import settingIdentifiers from '@constants/settings';
import subscribe from '@store/subscriber';
import initAligningGuidelines from '../extends/initAligningGuides';

const SceneManager = () => {
    const { dispatch, select, getState } = store;
    let fabricCanvas = null;
    let selection = null;
    const _resizeFunc = _function.throttle(() => _resize(), 50);
    let clipPathUnsubscribe;

    function init(canvasRef) {
        fabricCanvas = new fabric.Canvas(canvasRef.current, {
            preserveObjectStacking: true,
            backgroundColor: 'grey',
            controlsAboveOverlay: true,
            uniformScaling: false,
            centeredRotation: true,
            centeredScaling: true
        });

        window.addEventListener('resize', _resizeFunc);
        _resize(true);

        initAligningGuidelines(fabricCanvas);

        fabricCanvas.on('selection:created', _onSelectionCreated);
        fabricCanvas.on('selection:updated', _onSelectionUpdated);
        fabricCanvas.on('selection:cleared', _onSelectionCleared);
        fabricCanvas.on('object:depth:modified', _onObjectDepthModified);
        document.addEventListener('scene:actor:removed', _onActorRemoved);
        document.addEventListener('scene:actor:selected', _onActorSelected);
        document.addEventListener('scene:actor:deselected', _onActorDeselected);
        document.addEventListener(
            'scene:actor:deselected:multi',
            _onActorDeselectedMulti
        );
        document.addEventListener(
            'scene:selection:cleared',
            _onSceneSelectionCleared
        );

        const settings = getState().settings;
        _setClipPath(
            settings[settingIdentifiers.general.image.shape],
            settings[settingIdentifiers.general.image.size]
        );
        clipPathUnsubscribe = subscribe(
            'settings',
            null,
            (oldSettings, newSettings) => {
                let newImageShape =
                    newSettings[settingIdentifiers.general.image.shape];
                let newImageSize =
                    newSettings[settingIdentifiers.general.image.size];
                let oldImageShape = oldSettings
                    ? oldSettings[settingIdentifiers.general.image.shape]
                    : undefined;
                let oldImageSize = oldSettings
                    ? oldSettings[settingIdentifiers.general.image.size]
                    : undefined;
                let clipPath = fabricCanvas.clipPath;

                // Update clipPath type
                if (oldImageShape !== newImageShape) {
                    _setClipPath(newImageShape, newImageSize);
                }

                // Update clipPath size
                if (oldImageSize !== newImageSize) {
                    switch (newImageShape) {
                        case 'circle':
                            clipPath.set({ radius: newImageSize.radius });
                            break;
                        case 'rectangle':
                            clipPath.set({
                                width: newImageSize.width,
                                height: newImageSize.height
                            });
                            break;
                        default:
                            throw new Error(`Unhandled type: ${newImageShape}`);
                    }
                    clipPath.center();
                }
            }
        );

        console.log('SceneManager initialized');
        dispatch.scene.setInitialized(true);
    }

    function _setClipPath(shape, size) {
        let clipPath;
        switch (shape) {
            case 'circle':
                clipPath = new fabric.Circle(size);
                break;
            case 'rectangle':
                clipPath = new fabric.Rect(size);
                break;
            default:
                throw new Error(`Unhandled type: ${shape}`);
        }
        fabricCanvas.clipPath = clipPath;
        fabricCanvas.centerObject(clipPath);
    }

    function _resize(firstResize = false) {
        const parentContainer = fabricCanvas.getElement().parentElement
            .parentElement;
        const lastWidth = firstResize
            ? parentContainer.clientWidth
            : fabricCanvas.getWidth();
        const lastHeight = firstResize
            ? parentContainer.clientHeight
            : fabricCanvas.getHeight();
        const newWidth = parentContainer.clientWidth;
        const newHeight = parentContainer.clientHeight;
        const deltaX = (newWidth - lastWidth) * 0.5;
        const deltaY = (newHeight - lastHeight) * 0.5;

        fabricCanvas.setDimensions({
            width: newWidth,
            height: newHeight
        });

        // TODO: Restore current selection
        // clearSelectedActors();

        const group = new fabric.Group(fabricCanvas.getObjects());
        if (fabricCanvas.clipPath) group.addWithUpdate(fabricCanvas.clipPath);

        group.set({
            left: group.left + deltaX,
            top: group.top + deltaY
        });
        group.destroy();
    }

    function addActor(actor) {
        if (actor.fabricObject) fabricCanvas.add(actor.fabricObject);

        dispatch.scene.addActor(actor);
        _fireEvent('scene:actor:added', { detail: actor });

        return actor;
    }

    function removeActor(actor) {
        console.debug('Remove actor', actor);
        dispatch.scene.removeActor(actor);
        _fireEvent('scene:actor:removed', { detail: actor });
    }

    function removeActors(actors) {
        actors.forEach(actor => removeActor(actor));
    }

    function selectActor(actor, append = false) {
        if (!actor) return;

        // Check if actor is not already selected
        const isSelected = select.scene.isSelected(actor)(store.getState());
        if (isSelected) {
            if (append) return;

            const selectedActors = store.getState().scene.selectedActors;
            const toDeselect = selectedActors.filter(a => actor !== a);
            deselectActors(toDeselect);
        } else {
            if (!append) clearSelectedActors();

            console.debug('Select actor', actor, append);
            dispatch.scene.addSelectedActor(actor);

            _fireEvent('scene:actor:selected', { detail: actor });
        }
    }

    function selectActors(actors, append = false) {
        if (!Array.isArray(actors))
            throw new Error('actors parameter should be an array');

        if (actors.length === 0) return;

        if (!append) clearSelectedActors();

        console.debug('Select actors', actors, append);
        // Only select actors that are not already selected
        const filteredActors = actors.filter(
            actor => !select.scene.isSelected(actor)(store.getState())
        );

        dispatch.scene.addSelectedActors(filteredActors);
        _fireEvent('scene:actor:selected:multi', { detail: filteredActors });
    }

    function deselectActor(actor) {
        if (!actor) return;

        // Check if actor is not already deselected
        const isSelected = select.scene.isSelected(actor)(store.getState());
        if (!isSelected) return;

        console.debug('Deselect actor', actor);

        dispatch.scene.removeSelectedActor(actor);
        _updateMultiSelection();

        _fireEvent('scene:actor:deselected', { detail: actor });
    }

    function deselectActors(actors) {
        if (!Array.isArray(actors))
            throw new Error('actors parameter should be an array');

        if (actors.length === 0) return;

        // Only deselect actors that are not already deselected
        const filteredActors = actors.filter(actor =>
            select.scene.isSelected(actor)(store.getState())
        );

        console.debug('Deselect actors', filteredActors);

        dispatch.scene.removeSelectedActors(filteredActors);
        _updateMultiSelection();

        _fireEvent('scene:actor:deselected:multi', { detail: filteredActors });
    }

    function _updateMultiSelection() {
        const selectedActors = store.getState().scene.selectedActors;
        if (selectedActors.length === 1) {
            const selectedActor = selectedActors[0];
            if (selectedActor.fabricObject) {
                console.debug('Change group selection to single selection');
                fabricCanvas.setActiveObject(selectedActor.fabricObject);
            }
        }
    }

    function toggleActorSelection(actor) {
        const isSelected = select.scene.isSelected(actor)(store.getState());
        if (isSelected) deselectActor(actor);
        else selectActor(actor, true);
    }

    function clearSelectedActors() {
        console.debug('Clear actor selection');
        dispatch.scene.clearSelectedActors();

        _fireEvent('scene:selection:cleared');
    }

    function requestRenderAll() {
        fabricCanvas.requestRenderAll();
    }

    function cleanup() {
        document.removeEventListener('resize', _resizeFunc);
        document.removeEventListener('scene:actor:removed', _onActorRemoved);
        document.removeEventListener('scene:actor:selected', _onActorSelected);
        document.removeEventListener(
            'scene:actor:deselected',
            _onActorDeselected
        );
        document.removeEventListener(
            'scene:actor:deselected:multi',
            _onActorDeselectedMulti
        );
        document.removeEventListener(
            'scene:selection:cleared',
            _onSceneSelectionCleared
        );
        fabricCanvas.off('selection:created', _onSelectionCreated);
        fabricCanvas.off('selection:updated', _onSelectionUpdated);
        fabricCanvas.off('selection:cleared', _onSelectionCleared);
        fabricCanvas.off('object:depth:modified', _onObjectDepthModified);
        clipPathUnsubscribe();
    }

    function _onSelectionCreated(e) {
        const newSelection = fabricCanvas.getActiveObject();
        console.debug('Selection created', newSelection, e);

        // Multi selection
        if (newSelection.type === 'activeSelection') {
            _initFabricObject(fabricCanvas, newSelection);
            newSelection.fixPosition();

            selectActors(
                newSelection.getObjects().map(object => object.actor),
                true
            );
        } else {
            selectActor(newSelection.actor, true);
        }
        selection = newSelection;
    }

    function _onSelectionUpdated(options) {
        console.debug('Selection updated', options);

        const newSelection = fabricCanvas.getActiveObject();
        if (newSelection instanceof fabric.ActiveSelection && selection !== newSelection) {
            _initFabricObject(fabricCanvas, fabricCanvas.getActiveObject());
            newSelection.fixPosition();
        }

        selection = newSelection;

        if (options.deselected && options.deselected.length > 0)
            deselectActors(options.deselected.map(object => object.actor));
        if (options.selected && options.selected.length > 0)
            selectActors(
                options.selected.map(object => object.actor),
                true
            );
    }

    function _onSelectionCleared(e) {
        console.debug('Selection cleared', e);

        clearSelectedActors(false);
        selection = null;
    }

    function _onObjectDepthModified() {
        dispatch.scene.updateActors();
    }

    function _onActorRemoved(target) {
        const actor = target.detail ? target.detail : target;

        if (actor.fabricObject) fabricCanvas.remove(actor.fabricObject);
    }

    function _onActorSelected(target) {
        const actor = target.detail ? target.detail : target;
        const { getState } = store;
        const selectedActors = getState().scene.selectedActors;

        // If it's a fabric canvas actor, also select the object on canvas
        if (!actor.fabricObject) return;
        else if (selectedActors.length > 1) {
            if (selection && selection instanceof fabric.ActiveSelection) {
                console.debug('add actor to current selection');
                selection.addWithUpdate(actor.fabricObject);
            } else {
                console.debug('create new active selection');
                const activeSelection = new fabric.ActiveSelection(
                    selectedActors
                        .filter(actor => actor.fabricObject)
                        .map(actor => actor.fabricObject),
                    { canvas: fabricCanvas }
                );
                fabricCanvas.setActiveObject(activeSelection);
                selection = activeSelection;
            }
        } else if (selectedActors.length > 0) {
            fabricCanvas.setActiveObject(actor.fabricObject);
            selection = actor.fabricObject;
        } else return;

        requestRenderAll();
    }

    function _onActorDeselectedMulti(target) {
        const actors = target.detail ? target.detail : target;
        actors.forEach(actor => _onActorDeselected(actor));
    }

    function _onActorDeselected(target) {
        const actor = target.detail ? target.detail : target;
        
        if (!actor.fabricObject) return;
        else if (selection && selection instanceof fabric.ActiveSelection) {
            console.debug('remove actor(s) from current selection');
            if (Array.isArray(actor)) {
                actor.forEach(item =>
                    selection.removeWithUpdate(item.fabricObject)
                );
            } else {
                selection.removeWithUpdate(actor.fabricObject);
            }
        } else if (fabricCanvas.getActiveObject() === actor.fabricObject) {
            fabricCanvas.discardActiveObject(null);
        }

        requestRenderAll();
    }

    function _onSceneSelectionCleared() {
        console.debug('Scene selection cleared');

        fabricCanvas.discardActiveObject(null);
        selection = null;

        requestRenderAll();
    }

    function _fireEvent(name, init) {
        document.dispatchEvent(new CustomEvent(name, init));
    }

    return {
        init: init,
        addActor: addActor,
        removeActor: removeActor,
        removeActors: removeActors,
        selectActor: selectActor,
        selectActors: selectActors,
        deselectActor: deselectActor,
        deselectActors: deselectActors,
        toggleActorSelection: toggleActorSelection,
        clearSelectedActors: clearSelectedActors,
        requestRenderAll: requestRenderAll,
        cleanup: cleanup,
        get canvas() {
            return fabricCanvas;
        }
    };
};

export default SceneManager();
