const webpackAlias = require('./../webpack.aliases.js');

module.exports = function generateAliases(srcPath, isJest = false) {
    if (isJest) {
        return {
            '^@assets(.*)$': '<rootDir>/src/assets/$1',
            '^@components(.*)$': '<rootDir>/src/components/$1',
            '^@constants(.*)$': '<rootDir>/src/constants/$1',
            '^@factories(.*)$': '<rootDir>/src/factories/$1',
            '^@hooks(.*)$': '<rootDir>/src/hooks/$1',
            '^@polyfills(.*)$': '<rootDir>/src/polyfills/$1',
            '^@services(.*)$': '<rootDir>/src/services/$1',
            '^@store(.*)$': '<rootDir>/src/store/$1',
            '^@themes(.*)$': '<rootDir>/src/themes/$1',
            '^@utils(.*)$': '<rootDir>/src/utils/$1'
        };
    } else {
        return webpackAlias.resolve.alias;
    }
};
