const { paths } = require('react-app-rewired');
const generateAliases = require('./tools/webpackAliases.js');
const { override, addWebpackAlias, useBabelRc } = require('customize-cra');
const path = require('path');

module.exports = {
    webpack: override(
        addWebpackAlias(generateAliases(paths.appSrc)),
        useBabelRc()
    ),
    jest: function (config) {
        config.moduleNameMapper = generateAliases(
            path.join(process.cwd(), 'src'),
            true
        );
        return config;
    },
    devServer: function (configFunction) {
        return function (proxy, allowedHost) {
            const config = configFunction(proxy, allowedHost);

            config.watchOptions.ignored = ['**/node_modules/**', '**/.git/**'];
            return config;
        };
    }
};
