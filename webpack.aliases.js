/**
 * Hack for Webstorm webpack aliases resolving
 */

const path = require('path');
const cwd = process.cwd();
module.exports = {
    resolve: {
        alias: {
            '@assets': path.resolve(cwd, 'src/assets'),
            '@components': path.resolve(cwd, 'src/components'),
            '@constants': path.resolve(cwd, 'src/constants'),
            '@factories': path.resolve(cwd, 'src/factories'),
            '@hooks': path.resolve(cwd, 'src/hooks'),
            '@polyfills': path.resolve(cwd, 'src/polyfills'),
            '@services': path.resolve(cwd, 'src/services'),
            '@store': path.resolve(cwd, 'src/store'),
            '@themes': path.resolve(cwd, 'src/themes'),
            '@utils': path.resolve(cwd, 'src/utils')
        }
    }
};
